<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<? include 'dump.php'; ?>
<script>
    function jsonEscape(str)  {
        return str.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");
    }
    var list = [
    <?= file_get_contents('logs.js')?>
    ];
    list.forEach(function(el){
        try{
            console.log(JSON.parse(jsonEscape(el)));
        }catch(e){
            console.log('JSON parse ERROR');
        }
        
    });
</script>
</body>
</html>
<?php

include 'deliveme.php';
include '../lib/yapona.php';

$result = false;

switch ($_POST['type']){
    case 'disable':
        if(!$_POST['chat_id']) $result = array('error' => 'Missing chat_id');
        $result = array(
            'ok' => Deliveme::disableChat($_POST['chat_id'])
        );
        $lastAction = [
            'type' => '1',
        ];
        $bot->setLastAction($_POST['chat_id'], $lastAction);
        break;
    case 'enable':
        if($_POST['chat_id']){
            $result = array(
                'ok' => Deliveme::enableChat($_POST['chat_id'])
            );
            $bot = new Bot();
            $bot->sendDefaultKeyboard($_POST['chat_id'], "<b>Оператор отключился от чата</b>");
            $bot->setLastAction($_POST['chat_id'], "");
        }else{
            $result = array('error' => 'Missing chat_id');
        }
        break;
    case 'update_config':
        if(is_string($_POST['config'])){
            $result = array(
                'ok' => Deliveme::updateConfig(json_decode($_POST['config'], true))
            );
        }
        break;
    case 'get_config':
        $config = Deliveme::getConfig();
        $result = array(
            'ok' => !!$config,
            'config' => $config
        );
        break;
    default:
        $result = array(
            'error' => 'Unknown action'
        );
        break;
}
if($_GET['test']){
    $bot = new Bot();
    var_dump($bot->sendDefaultKeyboard(324083121, "<b>Оператор отключился от чата</b>"));
}

echo json_encode($result);
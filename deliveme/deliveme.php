<?php

class Deliveme{
    protected $module_dir;
    protected $curl;
    protected $base_url;
    public $request;
    public $response;
    public $apiResponse;
    public $additions = array();
    public $messages = array();
    
    public function __construct()
    {
        $config = self::getConfig();
        $this->base_url = $config['base_url'];
        
        $this->curl = curl_init();
        $this->clearDump();

        $request = $this->getRequest();
        $this->saveRequest($request);
    }
    public function __destruct()
    {
        $this->sendAll();
    }
    public function getRequest()
    {
        return json_decode(file_get_contents("php://input"), TRUE);
    }
    public function saveMessage()
    {
        array_push($this->messages, array(
            'request' => $this->request,
            'response' => $this->response,
            'apiResponse' => $this->apiResponse,
            'additions' => $this->additions
        ));

        $this->response = null;
        $this->apiResponse = null;
    }
    private function isJson($string){
        if(strpos($string, '}') == false) return false;

        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
    private function decodeDepthJson($value)
    {
        if(is_string($value) && $this->isJson($value)){
            $value = json_decode($value, true);
        }
        if(is_array($value)){
            foreach ($value as $k => $val){
                $value[$k] = $this->decodeDepthJson($val);
            }
        }
        return $value;
    }
    public function dump($var)
    {
        ob_start();
        var_dump($var);
        $content = ob_get_contents();
        ob_end_clean();

        file_put_contents(__DIR__.'/dump.php', $content, FILE_APPEND | LOCK_EX);
    }
    public function clearDump()
    {
        file_put_contents(__DIR__.'/dump.php', '');
    }
    public function saveRequest($request)
    {
        $this->request = $this->decodeDepthJson($request);
    }
    public function saveResponse($response)
    {
        if($this->response){
            $this->saveMessage();
        }
        $this->response = $this->decodeDepthJson($response);
    }
    public function saveApiResponse($response)
    {
        $this->apiResponse = $this->decodeDepthJson($response);
    }
    public function saveAdditions($key, $value)
    {
        $this->additions[$key] = $value;
    }
    public static function disableChat($chat_id)
    {
        return file_put_contents(__DIR__.'/disabled_chats/'.$chat_id.'.txt', 'disabled');
    }
    public static function enableChat($chat_id)
    {
        return unlink (__DIR__.'/disabled_chats/'.$chat_id.'.txt');
    }
    public static function updateConfig($ar)
    {
        return self::saveConfig(array_merge(self::getConfig(), $ar));
    }
    public static function getConfig()
    {
        return json_decode(file_get_contents(__DIR__.'/config.json'), true);
    }
    public static function saveConfig($config)
    {
        return file_put_contents(__DIR__.'/config.json', json_encode($config));
    }
    public function isBotActive()
    {
        if($this->request['sender']){ //It is Viber
            $chat_id = $this->request['sender']['id'];
        }else{
            if ($this->request['message']) {
                $chat_id = $this->request['message']["chat"]["id"];
            } else {
                $chat_id = $this->request["callback_query"]["message"]["chat"]["id"];
            }
        }
        return !file_exists(__DIR__.'/disabled_chats/'.$chat_id.'.txt');
    }
    public function sendAll(){
        $this->saveMessage();
        return $this->send($this->messages);
    }

    public function send($array)
    {
        $JSON = json_encode($array);


        $options = [
            CURLOPT_URL => $this->base_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => array('message' => $JSON)
        ];

        curl_setopt_array($this->curl, $options);
        $res = curl_exec($this->curl);

        $this->dump($res);

        $script = "'".addslashes($JSON)."',";

        return file_put_contents(__DIR__.'/logs.js', $script.PHP_EOL.PHP_EOL , FILE_APPEND | LOCK_EX);
    }
    public static function getDir(){
        return __DIR__;
    }
}
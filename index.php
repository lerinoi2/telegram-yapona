<?php

function isRequestUniq()
{
    $request = json_decode(file_get_contents("php://input"), TRUE);
    $id = $request['update_id'];
    $newRequest = true;

    $arr = json_decode(file_get_contents("log.json"), TRUE);
    if ($arr[$id]) {
        $arr[$id] += 1;
        $newRequest = false;
    } else {
        $arr[$id] = 1;
    }
    file_put_contents("log.json", json_encode($request));

    return $newRequest;
}

if (!isRequestUniq()) return;

include_once "lib/yapona.php";
include_once "config.php";

$bot = new Bot($botToken);

include_once "deliveme/deliveme.php";
global $DM;
$DM = new Deliveme();
$DM->saveAdditions('review', false);
$DM->saveAdditions('token', $botToken);

if (!$DM->isBotActive()) return;

$response = json_decode(file_get_contents("php://input"), TRUE);

if ($response['message']) {
    $message = $response['message'];
    $chatId = $message["chat"]["id"];
    $text = $message["text"];
} else {
    $message = $response["callback_query"]["message"];
    $chatId = $message["chat"]["id"];
    $data = json_decode($response["callback_query"]["data"]);
}

//$res = $bot->getOrderStatus('406736725');
//var_dump($res);

$keyboardType = false;

switch ($text) {
    case "/start":
        $name = $response["message"]["chat"]["first_name"] . " " . $response["message"]["chat"]["last_name"];
        $msg = "Добрый день, " . $name . "!\n\nЯ робот-ассистент службы доставки сети кафе Япона-Матрена г. Пермь, тел.: 2-700-706.\nЯ могу показать вам полное меню доставки или найти отдельное блюдо по названию или составу.\nС удовольствием приму у вас заказ, покажу его статус и приму Ваш отзыв. \n\nХорошего настроения! " . $bot->getEmoji('\xF0\x9F\x98\x89');
        $bot->addUser($chatId, $name);
        $bot->setBasket($chatId, []);
        $bot->sendSticker($chatId, 'CAADAgADDQADVU8-GDqaud5nWsuEAg', $keyboard, 'HTML');
        $bot->sendMessage($chatId, $msg, $keyboard, 'HTML');

        $res = $bot->getUserInfo($chatId);
        if (!$res['phone']) {
            $msg = $l['startNotNumberText'];
            $bot->sendMessage($chatId, $msg, $keyboard, 'HTML');
        }
    break;

    case $l['menuBtn']:
        $keyboard = [[$l['menuBtn'], $l['basketBtn']], [$l['statusBtn'], $l['reviewTextBtn']], [$l['saleBtn'], $l['operBtn']]];
        $bot->sendMessage($chatId, $bot->getEmoji('\xF0\x9F\x8D\xA3'), $keyboard, 'HTML');
        $msg = $l['menuText'];
        $menu = $bot->getMenu();
        $inlineKeyboard = array();
        foreach ($menu as $id => $section) {
            $inlineKeyboard[] = [array('text' => $section['EMOJI'] . ' ' . $section['NAME'], 'callback_data' => '{"type":"menu","value":' . $id . '}')];
        }
        $inlineKeyboard[] = [array(
            'text' => 'История заказов',
            'callback_data' => '{"type":"history"}'
        )];
        $bot->sendMessage($chatId, $msg, $inlineKeyboard, 'HTML', 'inline');
        $bot->setLastAction($chatId, '');
    break;

    case $l['saleBtn']:
        $bot->setLastAction($chatId, "");
        $res = $bot->getUserInfoSale($chatId);
        if (!$res['USER_FIO'] || !$res['BirthDay'] || !$res['phone']) {
            if (!$res['phone']) {
                $sendingData = $bot->getInfoDataSale("phone");
                $bot->sendMessage($chatId, $sendingData['message'], $sendingData['keyboard'], 'HTML', $sendingData['keyboardType']);
                $lastAction = ['action' => 'sale', 'res' => 'phone'];
                $bot->setLastAction($chatId, $lastAction);
                break;
            } elseif (!$res['USER_FIO']) {
                $bot->sendMessage($chatId, $l['saleText'], $keyboard, 'HTML');
                $lastAction = ['action' => 'sale', 'res' => 'USER_FIO'];
                $bot->setLastAction($chatId, $lastAction);
            } elseif (!$res['BirthDay']) {
                $bot->sendMessage($chatId, $l['BDayText'], $keyboard, 'HTML');
                $lastAction = ['action' => 'sale', 'res' => 'BirthDay'];
                $bot->setLastAction($chatId, $lastAction);
                break;
            }
        } else {
            $name = $response["message"]["chat"]["first_name"] . " " . $response["message"]["chat"]["last_name"];
            $sale = $bot->getSale($chatId);
            if (!is_null($sale["saleSize"])){
                if ($res['SALE_SIZE'] != $sale['saleSize']){
                    $bot->send_message($chatId, $name.", поздравляем Вас с достижением нового статуса в бонусной программе!");
                    $bot->send_message($chatId, "Ваш новый статус - ". $sale['name']);
                    $bot->send_message($chatId, " Это дает Вам скидку ".$sale['saleSize']."% при заказе с использованием Вашего номера телефона.");
                    $bot->send_message($chatId, " Хорошего дня! 🙂");
                    $bot->setUserInfo($chatId, 'SALE_SIZE', $sale['saleSize']);
                    $bot->setUserInfo($chatId, 'SALE_NAME', $sale['name']);
                }
            } else {
                if ((int)$res['SALE_SIZE'] > 5){
                    $bot->send_message($chatId, "Ваш новый статус - Silver");
                    $bot->send_message($chatId, " Это дает Вам скидку 5% при заказе с использованием Вашего номера телефона.");
                    $bot->send_message($chatId, " Хорошего дня! 🙂");
                    $bot->setUserInfo($chatId, 'SALE_SIZE', 5);
                    $bot->setUserInfo($chatId, 'SALE_NAME', 'Скидка при заказе через бота');
                }
            }
            $res = $bot->getUserInfoSale($chatId);
            $msg = "Ваше ФИО: " . $res["USER_FIO"] . "\nВаша дата рождения: " . $res["BirthDay"] . "\nВаш номер телефона: " . $res["phone"]."\n";
            $msg .= "Ваш статус - ". $res['SALE_NAME']."\n";
            $msg .= "Ваша скидка - ". $res['SALE_SIZE']."%";
            $bot->sendMessage($chatId, $msg, $keyboard, 'HTML');
            $inlineKeyboard[] = [array(
                'text' => 'Изменить контактные данные',
                'callback_data' => '{"type":"change_sale"}'
            )];
            $bot->sendMessage($chatId, "Хотите изменить Ваши данные?", $inlineKeyboard, 'HTML', 'inline');


        }
        break;

    case $l['backBtn']:
        $lastAction = $bot->getLastAction($chatId)['action'];
        if ($lastAction == 'sendOrder'){
            $view = $bot->getOrderView($chatId);
            $bot->sendMessage($chatId, $view['text'], $view['keyboard'], 'HTML');
            $inlineKeyboard[] = [array(
                'text' => 'Изменить контактные данные',
                'callback_data' => '{"type":"change_info"}'
            )];
            $bot->sendMessage($chatId, "Желаете изменить контактные данные?", $inlineKeyboard, 'HTML', 'inline');
        } elseif($lastAction == 'changeInfoSale'){
            $msg = $l['changeText'];
            $keyboard = $bot->getInfoKeyboardSale();
            $bot->sendMessage($chatId, $msg, $keyboard, 'HTML', 'inline');
        } else {
            $bot->sendMessage($chatId, "Назад", $keyboard, 'HTML');
        }
        $bot->setLastAction($chatId, "");
    break;

    case $l['operBtn']:
        if ($DM) {
            $DM->saveAdditions('call', true);
        }
        $inlineKeyboard[] = [array(
            'text' => 'Отключиться от чата',
            'callback_data' => '{"type": "back"}'
        )];
        $lastAction = [
            'type' => '1',
        ];
        $bot->setLastAction($chatId, $lastAction);
        $bot->sendMessage($chatId, $l['operText'], $inlineKeyboard, 'HTML', 'inline');
    break;

    case $l['basketBtn']:
        $basket = $bot->getBasket($chatId);
        if ($basket) {
            $name = $response["message"]["chat"]["first_name"] . " " . $response["message"]["chat"]["last_name"];
            $res = $bot->getUserInfoSale($chatId);
            $sale = $bot->getSale($chatId);
            if (!is_null($sale["saleSize"])){
                if ($res['SALE_SIZE'] != $sale['saleSize']){
                    $bot->sendMessage($chatId, $name.", поздравляем Вас с достижением нового статуса в бонусной программе!");
                    $bot->sendMessage($chatId, "Ваш новый статус - ". $sale['name']);
                    $bot->sendMessage($chatId, " Это дает Вам скидку ".$sale['saleSize']."% при заказе с использованием Вашего номера телефона.");
                    $bot->sendMessage($chatId, " Хорошего дня! 🙂");
                    $bot->setUserInfo($chatId, 'SALE_SIZE', $sale['saleSize']);
                    $bot->setUserInfo($chatId, 'SALE_NAME', $sale['name']);
                }
            } else {
                if ((int)$res['SALE_SIZE'] > 5){
                    $bot->sendMessage($chatId, "Ваш новый статус - Silver");
                    $bot->sendMessage($chatId, " Это дает Вам скидку 5% при заказе с использованием Вашего номера телефона.");
                    $bot->sendMessage($chatId, " Хорошего дня! 🙂");
                    $bot->setUserInfo($chatId, 'SALE_SIZE', 5);
                    $bot->setUserInfo($chatId, 'SALE_NAME', 'Скидка при заказе через бота');
                }
            }
            $keyboard = [[$l['orderBtn'], $l['backBtn']]];
            $msg = $l['basketText'];
            $titleId = $bot->sendMessage($chatId, $msg, $keyboard, 'HTML')['message_id'];

            foreach ($basket as $basketItem) {
                $view = $bot->getBasketProductView($basketItem['id'], $basketItem['o'], $basketItem['c']);
                $bot->sendPhoto($chatId, $view['IMG'], $keyboard);
                $bot->sendMessage($chatId, $view['text'], $view['keyboard'], 'HTML', 'inline');
            }

            $msg = "Сумма: <b>" . $bot->getBasketSumm($chatId) . " руб.</b>";
            if ($res['SALE_SIZE']) {
                $msg .= "\n(с учетом скидки)";
            }
            $inlineKeyboard[] = [array(
                'text' => $l['orderBtn'],
                'callback_data' => '{"type": "orderBtn"}'
            )];
            $bot->sendMessage($chatId, $msg, $inlineKeyboard, 'HTML', 'inline')['message_id'];
            $lastAction = [
                'action' => 'basket',
                'data' => [
                    'titleId' => $titleId,
                    'summId' => $summId,
                ]
            ];
            $bot->setLastAction($chatId, $lastAction);
        } else {
            $msg = $l['emptyBasketText'];
            $inlineKeyboard[] = [array(
                'text' => 'История заказов',
                'callback_data' => '{"type":"history"}'
            )];
            $bot->sendMessage($chatId, $msg, $inlineKeyboard, 'HTML', 'inline');
        }
    break;

    case $l['orderBtn']:
        $lastAction = $bot->getLastAction($chatId);
        if ($lastAction['action'] == 'sendOrder') {
            $res = $bot->createOrder($chatId, $lastAction['basket']);
            if ($res['result'] == 'success') {
                $keyboard = [[$l['menuBtn'], $l['statusBtn']]];
                if ($res['nextStep'] == 'payment') {
                    $msg = $l['orderSuccessText'];
                    $bot->sendMessage($chatId, $msg, $keyboard, 'HTML');
                    $msg = $l['orderPayText'];
                    $inlineKeyboard = [[["text" => "Оплатить", 'url' => $res['data']->formUrl]]];
                    $keyboardType = 'inline';
                    $bot->sendMessage($chatId, $msg, $inlineKeyboard,'HTML', $keyboardType);
                } else {
                    $msg = $l['orderSuccessText'];
                    $msg .= $l['orderSuccessInfoText'];
                    $bot->sendMessage($chatId, $msg, $keyboard, 'HTML');
                }
            } else {
                $msg = $l['orderErrText'];
                $keyboard = [[$l['sendBtn'], $l['basketBtn']], [$l['contactBtn']]];
                $bot->sendMessage($chatId, $msg, $keyboard, 'HTML');
            }
            $bot->setLastAction($chatId, "");
        } else {
            if ($bot->getBasketSumm($chatId) < 400) {
                $msg .= $l['minSummErrText'];
                $keyboard = [[$l['menuBtn'], $l['orderBtn']]];
                $bot->sendMessage($chatId, $msg, $keyboard, 'HTML');
            } else {
                $code = $bot->getEmptyUserInfo($chatId);
                if ($code) {
                    $lastAction = [
                        'action' => 'order',
                        'data' => [
                            'code' => $code,
                        ],
                    ];
                    $bot->setLastAction($chatId, $lastAction);
                    $sendingData = $bot->getInfoData($code);
                    $bot->sendMessage($chatId, $sendingData['message'], $sendingData['keyboard'], 'HTML', $sendingData['keyboardType']);
                } else {
                    $view = $bot->getOrderView($chatId);
                    $bot->sendMessage($chatId, $view['text'], $view['keyboard'], 'HTML');
                    $inlineKeyboard[] = [array(
                        'text' => 'Изменить контактные данные',
                        'callback_data' => '{"type":"change_info"}'
                    )];
                    $bot->sendMessage($chatId, "Желаете изменить контактные данные?", $inlineKeyboard, 'HTML', 'inline');
                }
            }
        }
        break;

    case $l['sendBtn']:
        $code = $bot->getEmptyUserInfo($chatId);
        if (!$code) {
            $basket = $bot->getBasket($chatId);
            if ($basket) {
                $keyboard = [[$l['orderBtn'], $l['backBtn']]];
                $bot->sendMessage($chatId, "Введите комментарий к заказу или нажмите на кнопку 'Оформить заказ'", $keyboard, 'HTML');
                $lastAction = [
                    'action' => 'sendOrder',
                    'basket' => $basket
                ];
                $bot->setLastAction($chatId, $lastAction);
            } else {
                $bot->sendMessage($chatId, $l['emptyBasketText'], $keyboard);
            }
        } else {
            $sendingData = $bot->getInfoData($code);
            $bot->sendMessage($chatId, $sendingData['message'], $sendingData['keyboard'], 'HTML', $sendingData['keyboardType']);
            $lastAction = [
                'action' => 'order',
                'data' => [
                    'code' => $code,
                ],
            ];
            $bot->setLastAction($chatId, $lastAction);
        }
    break;

    case $l['statusBtn']:
        $res = $bot->getUserInfo($chatId);
        if (!$res['phone']) {
            $sendingData = $bot->getInfoDataSale("phone");
            $bot->sendMessage($chatId, $sendingData['message'], $sendingData['keyboard'], 'HTML', $sendingData['keyboardType']);
            $lastAction = [
                'action' => 'first_number',
                'data' => 'phone',
            ];
            $bot->setLastAction($chatId, $lastAction);
        } else {
            $statusId = $bot->getOrderStatus($chatId);
            $pic = true;
            if ($statusId >= 1 and $statusId <= 3){
                $msg = 'Ваш заказ создан.';
                $pic = false;
            } elseif ($statusId >= 4 and $statusId <= 5){
                $msg = "CAADAgADCQADVU8-GKwOdn8lXxStAg";
            } elseif ($statusId == 6){
                $msg = "CAADAgADCgADVU8-GCCtlRwnrStmAg";
            } elseif ($statusId == 7){
                $msg = "CAADAgADCwADVU8-GKRgbk-SP0FZAg";
            } else {
                $msg = 'У Вас нет активных заказов.';
                $pic = false;
            }
            if ($pic) {
                $bot->sendSticker($chatId, $msg, $keyboard, 'HTML');
            } else {
                $bot->sendMessage($chatId, $msg, $keyboard, 'HTML');
            }
            $bot->setLastAction($chatId, '');
        }
        break;

    case $l['reviewTextBtn']:
        $msg = $l['reviewText_new'];
        $star = $bot->getEmoji('\xE2\xAD\x90');
        $arr = array();
        for ($i = 1; $i <= 5; $i++) {
            $arr[] = [
                'text' => $i . " " . $star,
                'callback_data' => '{"type":"review_star","value":"' . $i . '"}'
            ];
        }
        $inlineKeyboard = array($arr);
        $DM->saveAdditions('review', true);
        $bot->sendMessage($chatId, $msg, $inlineKeyboard, 'HTML', 'inline');
//        $bot->track($message, $text);
    break;

    case $l["review_sendBtn"]:
        $bot->setLastAction($chatId, "");
        $msg = $l['reviewThankText'];
        if ($DM) $DM->saveAdditions('last_review_message', true);
        if ($DM) $DM->saveAdditions('review', true);
        $bot->sendMessage($chatId, $msg, $keyboard, 'HTML');
        break;

    case $l['contactBtn']:
        $msg = $l['changeText'];
        $keyboard = $bot->getInfoKeyboard();
        $bot->sendMessage($chatId, $msg, $keyboard, 'HTML', 'inline');
        $bot->setLastAction($chatId, '');
    break;

    case $l['resetBtn']:
        $msg = $l['cancelText'];
        $keyboard = [[$l['orderBtn'], $l['basketBtn']]];
        $bot->sendMessage($chatId, $msg, $keyboard, 'HTML');
        $bot->sendDefaultChange($chatId);
        $bot->setLastAction($chatId, '');
    break;

    default:
        if ($data->type == null) {
            $lastAction = $bot->getLastAction($chatId);
            if ($lastAction['action'] == 'order' && $lastAction['data']['code']) {
                if ($lastAction['data']['code'] == 'phone' && $text == '') {
                    $text = $response['message']['contact']['phone_number'];
                }
                if ($lastAction['data']['code'] == 'phone') {
                    $value = str_replace("+7", "8", $text);
                    $value = preg_replace('/(\(|\)|-|\s|\s+)/', '', $value);
                    if($value[0] == "7") {
                        $value = "8".substr($value, 1);
                    }
                    if ($DM) $DM->saveAdditions('phone', $value);
                }
                $bot->setUserInfo($chatId, $lastAction['data']['code'], $text);
                $code = $bot->getEmptyUserInfo($chatId);
                if ($code) {
                    $sendingData = $bot->getInfoData($code);
                    $bot->sendMessage($chatId, $sendingData['message'], $sendingData['keyboard'], 'HTML', $sendingData['keyboardType']);
                    $lastAction = [
                        'action' => 'order',
                        'data' => [
                            'code' => $code,
                        ],
                    ];
                    $bot->setLastAction($chatId, $lastAction);
                } else {
                    $view = $bot->getOrderView($chatId);
                    $bot->sendMessage($chatId, $view['text'], $view['keyboard'], 'HTML');
                    $inlineKeyboard[] = [array(
                        'text' => 'Изменить контактные данные',
                        'callback_data' => '{"type":"change_info"}'
                    )];
                    $bot->sendMessage($chatId, "Желаете изменить контактные данные?", $inlineKeyboard, 'HTML', 'inline');
                    $bot->setLastAction($chatId, '');
                }
            }
            elseif ($lastAction['action'] == 'changeInfo') {
                if ($lastAction['data']['code']) {
                    if ($lastAction['data']['code'] == 'phone' && $text == '') {
                        $text = $response['message']['contact']['phone_number'];
                    }

                    if ($lastAction['data']['code'] == 'phone') {
                        $value = str_replace("+7", "8", $text);
                        $value = preg_replace('/(\(|\)|-|\s|\s+)/', '', $value);
                        if($value[0] == "7") {
                            $value = "8".substr($value, 1);
                        }
                        if ($DM) $DM->saveAdditions('phone', $value);
                    }
                    $bot->setUserInfo($chatId, $lastAction['data']['code'], $text);
                    $bot->sendDefaultChange($chatId);
                    $bot->setLastAction($chatId, '');
                }
            }
            elseif ($lastAction['action'] == 'changeInfoSale') {
                if ($lastAction['data']['code']) {
                    if ($lastAction['data']['code'] == 'phone') {
                        $text = $response['message']['contact']['phone_number'];
                    }
                    if ($lastAction['data']['code'] == 'phone') {
                        $value = str_replace("+7", "8", $text);
                        $value = preg_replace('/(\(|\)|-|\s|\s+)/', '', $value);
                        if($value[0] == "7") {
                            $value = "8".substr($value, 1);
                        }
                        if ($DM) $DM->saveAdditions('phone', $value);
                    }
                    $bot->setUserInfo($chatId, $lastAction['data']['code'], $text);
                    $keyboard = $bot->getInfoKeyboardSale();
                    $bot->sendMessage($chatId, $l['changeText'], $keyboard, 'HTML', 'inline');
                    $bot->setLastAction($chatId, '');
                }
            }
            elseif ($lastAction['action'] == 'review') {
                $lastAction = '';
                if ($message['photo']) {
                    $last_rez = array_pop($message["photo"]);
                    $path = $bot->getFile($last_rez["file_id"]);
                    $link = "https://api.telegram.org/file/bot589421991:AAHxLmLnO4eCwRZUwQTbhpIpPHIplKtrMDQ/" . $path["file_path"];
                    $lastAction = [
                        'action' => 'review',
                        'data' => [
                            'type' => 'pic',
                            'path' => $link,
                        ],
                    ];
                    $bot->setLastAction($chatId, $lastAction);
                } else {
                    $lastAction = [
                        'action' => 'review',
                        'data' => [
                            'type' => 'text_more'
                        ],
                    ];
                    $bot->setLastAction($chatId, $lastAction);
                }
                $res = $bot->setReview($chatId, $text);
                $DM->saveAdditions('review', true);
            }
            elseif ($lastAction['action'] == 'first_number') {
                if ($text == '') {
                    $text = $response['message']['contact']['phone_number'];
                }
                if ($lastAction['data']['code'] == 'phone') {
                    $value = str_replace("+7", "8", $text);
                    $value = preg_replace('/(\(|\)|-|\s|\s+)/', '', $value);
                    if($value[0] == "7") {
                        $value = "8".substr($value, 1);
                    }
                    if ($DM) $DM->saveAdditions('phone', $value);
                }
                $bot->setUserInfo($chatId, "phone", $text);
                $statusId = $bot->getOrderStatus($chatId);
                $pic = true;
                if ($statusId >= 1 and $statusId <= 3){
                    $msg = 'Ваш заказ создан.';
                    $pic = false;
                } elseif ($statusId >= 4 and $statusId <= 5){
                    $msg = "CAADAgADCQADVU8-GKwOdn8lXxStAg";
                } elseif ($statusId == 6){
                    $msg = "CAADAgADCgADVU8-GCCtlRwnrStmAg";
                } elseif ($statusId == 7){
                    $msg = "CAADAgADCwADVU8-GKRgbk-SP0FZAg";
               // } elseif ($statusId == 8) {
              //      $msg = "CAADAgADDAADVU8-GCCnJeGOtH8sAg";
                }
                else {
                    $msg = 'Спасибо, теперь вы можете перейти в меню и заказать вашу любимую еду';
                    $pic = false;
                }
                if ($pic) {
                    $bot->sendSticker($chatId, $msg, $keyboard, 'HTML');
                } else {
                    $bot->sendMessage($chatId, $msg, $keyboard, 'HTML');
                }
                $bot->setLastAction($chatId, '');
            }
            elseif ($lastAction['action'] == 'sale') {
                $res = $bot->getUserInfoSale($chatId);
                if ($lastAction['res'] == 'phone') {
                    $text = $response['message']['contact']['phone_number'];
                    if ($DM) {
                        $value = str_replace("+7", "8", $text);
                        $value = preg_replace('/(\(|\)|-|\s|\s+)/', '', $value);
                        if($value[0] == "7") {
                            $value = "8".substr($value, 1);
                        }
                        $DM->saveAdditions('phone', $value);
                    }
                    $bot->setUserInfo($chatId, 'phone', $text);
                    if ($res['USER_FIO'] == 0) {
                        $lastAction = ['action' => 'sale', 'res' => 'USER_FIO'];
                        $bot->setLastAction($chatId, $lastAction);
                        $bot->sendMessage($chatId, $l['saleText'], $keyboard, 'HTML');
                    } elseif (!$res['BirthDay'] && $res['USER_FIO'] != 0) {
                        $lastAction = ['action' => 'sale', 'res' => 'BirthDay'];
                        $bot->setLastAction($chatId, $lastAction);
                        $bot->sendMessage($chatId, $l['BDayText'], $keyboard, 'HTML');
                    } else {
                        $bot->setLastAction($chatId, "");
                    }
                    break;
                }
                if ($lastAction['res'] == 'USER_FIO') {
                    $text = $response['message']['text'];
                    $bot->setUserInfo($chatId, 'USER_FIO', $text);
                    $bot->sendMessage($chatId, $l['BDayText'], $keyboard, 'HTML');
                    $lastAction = ['action' => 'sale', 'res' => 'BirthDay'];
                    $bot->setLastAction($chatId, $lastAction);
                    break;
                }
                if ($lastAction['res'] == 'BirthDay') {
                    $bot->setUserInfo($chatId, 'BirthDay', $text);

                    $ress = $bot->getSale($chatId);
                    if (!is_null($ress['saleSize'])){
                        $bot->setUserInfo($chatId, 'SALE_SIZE', $ress['saleSize']);
                        $bot->setUserInfo($chatId, 'SALE_NAME', $ress['name']);
                    } else {
                        $bot->setUserInfo($chatId, 'SALE_SIZE', '5');
                        $bot->setUserInfo($chatId, 'SALE_NAME', 'Скидка через чат-бота');
                    }
                    $res = $bot->getUserInfoSale($chatId);
                    $bot->sendMessage($chatId, "Ваше ФИО: " . $res["USER_FIO"] . "\nВаша дата рождения: " . $res["BirthDay"] . "\nВаш номер телефона: " . $res["phone"]);
                    $bot->sendMessage($chatId, "Ваша скидка: " . $res["SALE_SIZE"] . "%\nВаш статус: ".$res['SALE_NAME']);

//                    $text = $response['message']['text'];
//                    $bot->setUserInfo($chatId, 'SALE_SIZE', '5');
//
//
//                    $bot->setLastAction($chatId, "");
//                    $res = $bot->getUserInfoSale($chatId);
//                    $msg = "Ваше ФИО: " . $res["USER_FIO"] . "\nВаша дата рождения: " . $res["BirthDay"] . "\nВаш номер телефона: " . $res["phone"];
//                    $msg .= "\nCкидка(при заказе через мессенджеры): " . $res['SALE_SIZE'] . "%";
//                    $bot->sendMessage($chatId, $msg, $keyboard, 'HTML');
                }
            }
            elseif ($lastAction['action'] == 'sendOrder') {
                $bot->sendMessage($chatId, 'Мы создаем Ваш заказ', $keyboard, 'HTML');
                $res = $bot->createOrder($chatId, $lastAction['basket'], $text);
//                $bot->sendMessage($chatId, $bot->log($res), $keyboard, 'HTML');
                if ($res['result'] == 'success') {
                    $keyboard = [[$l['menuBtn'], $l['statusBtn']]];
                    if ($res['nextStep'] == 'payment') {
                        $msg = $l['orderSuccessText'];
                        $bot->sendMessage($chatId, $msg, $keyboard, 'HTML');
                        $msg = $l['orderPayText'];
                        $inlineKeyboard = [[["text" => "Оплатить", 'url' => $res['data']->formUrl]]];
                        $keyboardType = 'inline';
                        $bot->sendMessage($chatId, $msg, $inlineKeyboard,'HTML', $keyboardType);
                    } else {
                        $msg = $l['orderSuccessText'];
                        $msg .= $l['orderSuccessInfoText'];
                        $bot->sendMessage($chatId, $msg, $keyboard, 'HTML');
                    }
                } else {
                    $msg = $l['orderErrText'];
                    $keyboard = [[$l['sendBtn'], $l['basketBtn']], [$l['contactBtn']]];
                    $bot->sendMessage($chatId, $msg, $keyboard, 'HTML');

                }
                $bot->setLastAction($chatId, "");
            } elseif ($lastAction['type'] == '1'){
                $inlineKeyboard[] = [array(
                    'text' => 'Отключиться от чата',
                    'callback_data' => '{"type": "back"}'
                )];
                $lastAction = [
                    'type' => '1',
                ];
                $bot->setLastAction($chatId, $lastAction);
                $bot->sendMessage($chatId, 'Отключиться от чата с оператором', $inlineKeyboard, 'HTML', 'inline');
            }
            else {
                $res = $bot->find($text);
                if ($res['msg']) {
                    $bot->sendMessage($chatId, $res['msg'], $keyboard, 'HTML');
                    break;
                }
                $countProducts = count($res['products']);
                $countSections = count($res['sections']);
                if (($countProducts != 0) || ($countSections != 0)) {
                    $sum = $countProducts + $countSections;
                    $msg = $bot->getEmoji('\xF0\x9F\x94\x8D') . ' <i>По Вашему запросу ' .
                        $bot->declOfNum($sum, ['найден', 'найдено', 'найдено']) . '</i> <b>' . ($sum) . '</b> <i>' .
                        $bot->declOfNum($sum, ['результат', 'результата', 'результатов']) . '</i>';
                    $bot->sendMessage($chatId, $msg, $keyboard, 'HTML');
                } else {
                    $msg = $l['searchErrText'];
                    $bot->sendMessage($chatId, $msg, $keyboard, 'HTML');
                }
                if ($countProducts != 0) {
                    $inlineKeyboard = array();
                    $msg = $bot->declOfNum($countProducts, ['Найденный', 'Найденные', 'Найденные']) . ' ' .
                        $bot->declOfNum($sum, ['товар', 'товары (<b>' . $countProducts . '</b>)', 'товары (<b>' . $countProducts . '</b>)']) . ': ';
                    for ($i = 0; $i < ($countProducts < $bot::PAGE_SIZE ? $countProducts : $bot::PAGE_SIZE); $i++) {
                        $inlineKeyboard[] = [array(
                            'text' => $res['products'][$i]['NAME'],
                            'callback_data' => '{"type":"product","value":' . $res['products'][$i]['ID'] . '}'
                        )];
                    }
                    if ($countProducts > $bot::PAGE_SIZE) {
                        $inlineKeyboard[] = [array(
                            'text' => 'Показать ещё',
                            'callback_data' => '{"type":"more","limit":' . $bot::PAGE_SIZE . '}'
                        )];
                        $lastAction = [
                            'action' => 'find_more',
                            'data' => $text,
                        ];
                        $bot->setLastAction($chatId, $lastAction);
                    }
                    $bot->sendMessage($chatId, $msg, $inlineKeyboard, 'HTML', 'inline');
                }
                if ($countSections != 0) {
                    $inlineKeyboard = array();
                    $msg = $bot->declOfNum($countSections, ['Найденная', 'Найденные', 'Найденные']) . ' ' .
                        $bot->declOfNum($sum, ['категория', 'категории (<b>' . $countSections . '</b>)', 'категории (<b>' . $countSections . '</b>)']) . ': ';
                    foreach ($res['sections'] as $section) {
                        $inlineKeyboard[] = [array(
                            'text' => $section['NAME'],
                            'callback_data' => '{"type":"section","value":' . $section['ID'] . '}'
                        )];
                    }
                    $bot->sendMessage($chatId, $msg, $inlineKeyboard, 'HTML', 'inline');
                }
            }
        }
}

switch ($data->type) {

    case 'change_info':
        $msg = $l['changeText'];
        $keyboard = $bot->getInfoKeyboard();
        $bot->sendMessage($chatId, $msg, $keyboard, 'HTML', 'inline');
        $bot->setLastAction($chatId, '');
    break;

    case 'changeInfoSale':
        $sendingData = $bot->getInfoDataSale($data->code);
        $bot->sendMessage($chatId, $sendingData['message'], $sendingData['keyboard'], 'HTML', $sendingData['keyboardType']);
        $lastAction = [
            'action' => 'changeInfoSale',
            'data' => [
                'code' => $data->code,
            ],
        ];
        $bot->setLastAction($chatId, $lastAction);
        break;

    case 'review_star':
        $res = $bot->setReview($chatId, (int)$data->value[0]);
        if ($DM) {
            $DM->saveAdditions('review', true);
            $DM->saveAdditions('stars', (int)$data->value[0]);
        }
        $msg = $l['reviewMoreText'];
        $lastAction = [
            'action' => 'review',
            'data' => [
                'type' => 'text'
            ],
        ];
        $bot->setLastAction($chatId, $lastAction);
        $msg = 'Пожалуйста, введите Ваш отзыв.';
        $keyboard = [[$l["review_sendBtn"]]];
        $bot->sendMessage($chatId, $msg, $keyboard, 'HTML');
        $bot->sendMessage($chatId, $l['reviewMoreTextPhoto'], $keyboard, 'HTML');
        break;

    case 'change_sale':
        $msg = $l['changeText'];
        $keyboard = $bot->getInfoKeyboardSale();
        $bot->sendMessage($chatId, $msg, $keyboard, 'HTML', 'inline');
    break;

    case 'menu':
        $sectionId = (int)$data->value;
        $sections = $bot->getSection($sectionId);
        $inlineKeyboard = array();
        foreach ($sections as $id => $section) {
            $inlineKeyboard[] = [array('text' => $section['NAME'], 'callback_data' => '{"type":"section","value":' . $id . '}')];
        }
//        $inlineKeyboard[] = [array('text' => 'Назад', 'callback_data' => '{"type":"back"}')];
        $bot->editMessageReplyMarkup($chatId, $message['message_id'], $inlineKeyboard, 'inline');
    break;

    case 'back':
        if ($DM) {
            $DM->saveAdditions('close', true);
        }
        $bot->sendMessage($chatId, "Вы отключились от чата с оператором", $keyboard, "HTML");
        $bot->setLastAction($chatId, "");
    break;

    case 'section':
        $sectionId = (int)$data->value;
        $res = $bot->getProducts($sectionId);
        $check = count($res['ITEMS']);
        $msg = '<i>' . $res['SECTION'] . '</i>';
        $bot->sendMessage($chatId, $msg, $keyboard, 'HTML');
        if ($check >= 20) {
            $msg = "Хотите увидеть больше товаров?";
            $prod = array_slice($res['ITEMS'], 0, 19);
            $inlineKeyboard = array();
            foreach ($prod as $product) {
                $bot->sendProduct($chatId, $product);
            }
            $lastAction = [
                'action' => 'more_prod',
                'data' => [
                    'count' => 19,
                    'sectionID' => $sectionId,
                ],
            ];
            $bot->setLastAction($chatId, $lastAction);
            $inlineKeyboard_more[] = [array(
                'text' => 'Показать еще',
                'callback_data' => '{"type": "more_prod"}'
            )];
            $bot->sendMessage($chatId, $msg, $inlineKeyboard_more, 'HTML', 'inline');
        } else {
            $inlineKeyboard = array();
            foreach ($res['ITEMS'] as $product) {
                $bot->sendProduct($chatId, $product, $keyboard);
            }
        }
    break;

    case 'product':
        $productId = (int)$data->value;
        $product = $bot->getProduct($productId);
        $bot->sendProduct($chatId, $product);
    break;

    case 'more':
        $limit = $data->limit + $bot::PAGE_SIZE;
        $lastAction = $bot->getLastAction($chatId);
        $res = $bot->find($lastAction['data']);
        $countProducts = count($res['products']);
        $inlineKeyboard = array();
        for ($i = 0; $i < ($countProducts < $limit ? $countProducts : $limit); $i++) {
            $inlineKeyboard[] = [array(
                'text' => $res['products'][$i]['NAME'],
                'callback_data' => '{"type":"product","value":' . $res['products'][$i]['ID'] . '}'
            )];
        }
        if ($countProducts > $limit) {
            $inlineKeyboard[] = [array(
                'text' => 'Показать ещё',
                'callback_data' => '{"type":"more","query":"' . $data->query . '","limit":"' . $limit . '"}'
            )];
        }
        $bot->editMessageReplyMarkup($chatId, $response['callback_query']['message']['message_id'], $inlineKeyboard, 'inline');
    break;

    case 'orderAdd':
        $count = $bot->addProduct($chatId, $data->productId, $data->mod);
        $count_prod = $bot->getBasketCount($chatId);
        $sum = $bot->getBasketSumm($chatId);
        $product = $bot->getProduct($data->productId);
        $inlineKeyboard[] = [array(
            'text' => 'Перейти в корзину',
            'callback_data' => '{"type":"basketBtn"}'
        )];
        $msg = "Товар: " . $product['NAME'] . ", добавлен в корзину.\nВ корзине: " . $count_prod . " " . $bot->declOfNum($count_prod, ['товар', 'товара', 'товаров']) . "\nОбщая сумма корзины: " . $sum . " " . $bot->declOfNum($sum, ['рублей', 'рублей', 'рублей']);
        $bot->sendMessage($chatId, $msg, $inlineKeyboard, 'HTML', 'inline');
    break;

    case 'orderRemove':
        $count = $bot->removeProduct($chatId, $data->productId, $data->mod);
        $lastAction = $bot->getLastAction($chatId);
        if ($count == 0) {
            $bot->deleteMessage($chatId, $response['callback_query']['message']['message_id']);
            $bot->updateSumm($chatId, $lastAction);
            if ($bot->getBasket($chatId) == false) {
                if ($lastAction['action'] == 'basket') {
                    $bot->deleteMessage($chatId, $lastAction['data']['summId']);
                    $res = $bot->editMessageText($chatId, $lastAction['data']['titleId'], $l['emptyBasketText'], $keyboard);
                    if (!$res) {
                        $bot->deleteMessage($chatId, $lastAction['data']['titleId']);
                        $bot->sendMessage($chatId, $l['emptyBasketText'], $keyboard, 'HTML');
                    }
                }
            }
        } else {
            $inlineKeyboard[] = [array(
                'text' => 'Перейти в корзину',
                'callback_data' => '{"type":"basketBtn"}'
            )];

            $bot->sendMessage($chatId, 'Кол-во товара уменьшено. В корзине: ' . $count, $inlineKeyboard, 'HTML', 'inline');
        }
        break;

    case "setInfo":
        $bot->setUserInfo($chatId, $data->code, $data->value);
        $code = $bot->getEmptyUserInfo($chatId);
        if ($code) {
            $sendingData = $bot->getInfoData($code);
            $bot->sendMessage($chatId, $sendingData['message'], $sendingData['keyboard'], 'HTML', $sendingData['keyboardType']);
            $lastAction = [
                'action' => 'order',
                'data' => [
                    'code' => $code,
                ],
            ];
            $bot->setLastAction($chatId, $lastAction);
        } else {
            $view = $bot->getOrderView($chatId);
            $bot->sendMessage($chatId, $view['text'], $view['keyboard'], 'HTML');
            $inlineKeyboard[] = [array(
                'text' => 'Изменить контактные данные',
                'callback_data' => '{"type":"change_info"}'
            )];
            $bot->sendMessage($chatId, "Желаете изменить контактные данные?", $inlineKeyboard, 'HTML', 'inline');
            $bot->setLastAction($chatId, '');
        }
        break;

    case "changeInfo":
        if ($data->value) {
            $bot->setUserInfo($chatId, $data->code, $data->value);
            $bot->sendDefaultChange($chatId);
            $bot->setLastAction($chatId, '');
        } else {
            $sendingData = $bot->getInfoData($data->code, 'change');
            $bot->sendMessage($chatId, $sendingData['message'], $sendingData['keyboard'], 'HTML', $sendingData['keyboardType']);
            $lastAction = [
                'action' => 'changeInfo',
                'data' => [
                    'code' => $data->code,
                ],
            ];
            $bot->setLastAction($chatId, $lastAction);
        }
    break;

    case 'orderBtn':
        if ($bot->getBasketSumm($chatId) < 400) {
            $msg .= $l['minSummErrText'];
            $keyboard = [[$l['menuBtn'], $l['orderBtn']]];
            $bot->sendMessage($chatId, $msg, $keyboard, 'HTML');
        } else {
            $code = $bot->getEmptyUserInfo($chatId);
            if ($code) {
                $lastAction = [
                    'action' => 'order',
                    'data' => [
                        'code' => $code,
                    ],
                ];
                $bot->setLastAction($chatId, $lastAction);
                $sendingData = $bot->getInfoData($code);
                $bot->sendMessage($chatId, $sendingData['message'], $sendingData['keyboard'], 'HTML', $sendingData['keyboardType']);
            } else {
                $view = $bot->getOrderView($chatId);
                $bot->sendMessage($chatId, $view['text'], $view['keyboard'], 'HTML');
                $inlineKeyboard[] = [array(
                    'text' => 'Изменить контактные данные',
                    'callback_data' => '{"type":"change_info"}'
                )];
                $bot->sendMessage($chatId, "Желаете изменить контактные данные?", $inlineKeyboard, 'HTML', 'inline');
            }
        }
//        $bot->track($message, $text);
        break;

    case 'basketBtn':
        $basket = $bot->getBasket($chatId);
        if ($basket) {
            $name = $response["message"]["chat"]["first_name"] . " " . $response["message"]["chat"]["last_name"];
            $res = $bot->getUserInfoSale($chatId);
            $sale = $bot->getSale($chatId);
            if (!is_null($sale["saleSize"])){
                if ($res['SALE_SIZE'] != $sale['saleSize']){
                    $bot->sendMessage($chatId, $name.", поздравляем Вас с достижением нового статуса в бонусной программе!");
                    $bot->sendMessage($chatId, "Ваш новый статус - ". $sale['name']);
                    $bot->sendMessage($chatId, " Это дает Вам скидку ".$sale['saleSize']."% при заказе с использованием Вашего номера телефона.");
                    $bot->sendMessage($chatId, " Хорошего дня! 🙂");
                    $bot->setUserInfo($chatId, 'SALE_SIZE', $sale['saleSize']);
                    $bot->setUserInfo($chatId, 'SALE_NAME', $sale['name']);
                }
            } else {
                if ((int)$res['SALE_SIZE'] > 5){
                    $bot->sendMessage($chatId, "Ваш новый статус - Silver");
                    $bot->sendMessage($chatId, " Это дает Вам скидку 5% при заказе с использованием Вашего номера телефона.");
                    $bot->sendMessage($chatId, " Хорошего дня! 🙂");
                    $bot->setUserInfo($chatId, 'SALE_SIZE', 5);
                    $bot->setUserInfo($chatId, 'SALE_NAME', 'Скидка при заказе через бота');
                }
            }
            $keyboard = [[$l['orderBtn'], $l['backBtn']]];
            $msg = $l['basketText'];
            $titleId = $bot->sendMessage($chatId, $msg, $keyboard, 'HTML')['message_id'];
            foreach ($basket as $basketItem) {
                $view = $bot->getBasketProductView($basketItem['id'], $basketItem['o'], $basketItem['c']);
                $bot->sendPhoto($chatId, $view['IMG'], $keyboard);
                $bot->sendMessage($chatId, $view['text'], $view['keyboard'], 'HTML', 'inline');
            }
            $msg = 'Сумма: <b>' . $bot->getBasketSumm($chatId) . ' руб.</b>';
            if ($res['SALE_SIZE']) {
                $msg .= "\n(с учетом скидки)";
            }
            $inlineKeyboard[] = [array(
                'text' => $l['orderBtn'],
                'callback_data' => '{"type": "orderBtn"}'
            )];
            $bot->sendMessage($chatId, $msg, $inlineKeyboard, 'HTML', 'inline')['message_id'];
            $lastAction = [
                'action' => 'basket',
                'data' => [
                    'titleId' => $titleId,
                    'summId' => $summId,
                ]
            ];
            $bot->setLastAction($chatId, $lastAction);
        } else {
            $msg = $l['emptyBasketText'];
            $bot->sendMessage($chatId, $msg, $keyboard, 'HTML')['message_id'];
        }
//        $bot->track($message, $text);
        break;

    case 'review':
        $msg = $l['reviewText_new'];
        $star = $bot->getEmoji('\xE2\xAD\x90');
        $arr = array();
        for ($i = 1; $i <= 5; $i++) {
            $arr[] = [
                'text' => $i . " " . $star,
                'callback_data' => '{"type":"review_star","value":"' . $i . '"}'
            ];
        }
        $inlineKeyboard = array($arr);
        $DM->saveAdditions('review', true);
        $bot->sendMessage($chatId, $msg, $inlineKeyboard, 'HTML', 'inline');
//        $bot->track($message, $text);
        break;

    case 'history':
        $count = $bot->historyProd($chatId);
        if ($count) {
            $bot->sendMessage($chatId, "Ваши последние заказы");
            $inlineKeyboard = array();
            for ($i = 1; $i <= $count; $i++) {
                $bot->historyProd($chatId, $i - 1);
                $basket = $bot->getLastAction($chatId);
                foreach ($basket as $basketItem) {
                    $view = $bot->getHistoryBasketProductView($basketItem['id'], $basketItem['o'], $basketItem['c']);
                    $bot->sendPhoto($chatId, $view['IMG'], $keyboard);
                    $bot->sendMessage($chatId, $view['text'], $view['keyboard'], 'HTML', 'inline');
                }
                $msg = "Сумма: <b>" . $bot->getSummLastAction($chatId) . " руб.</b>";
                $res = $bot->getUserInfoSale($chatId);
                if ($res['SALE_SIZE']) {
                    $msg .= "\n(с учетом скидки)";
                }
                $test = $i - 1.;
                $inlineKeyboard[] = [array(
                    'text' => "Добавить в корзину",
                    'callback_data' => '{"type": "historyadd", "bask": "' . $test . '"}'
                )];
                $bot->sendMessage($chatId, $msg, $inlineKeyboard, 'HTML', 'inline');
            }
        } else {
            $bot->sendMessage($chatId, 'Ваша история заказов пуста', $keyboard, 'HTML');
        }
    break;

    case 'historyadd':
        $basket = $bot->historyProd($chatId, $data->bask);
        $bot->basketTolastAction($chatId, $basket);
        $bot->sendMessage($chatId, "Товары добавлены в корзину", $inlineKeyboard, 'HTML', 'inline');
    break;

    case 'more_prod':
        $lastAction = $bot->getLastAction($chatId);
        $res = $bot->getProducts($lastAction['data']['sectionID']);
        $check = count($res['ITEMS']);
        $count_prev = $lastAction['data']['count'];
        $count = 20;
        $count_next = $count_prev + $count;
        $checker = ($count_next + 1) - $count;
        if ($check >= $checker) {
            $more = $count_next + 1;
            $prod = array_slice($res['ITEMS'], $count_prev, $count);
            $msg = "Хотите увидеть больше товаров?";
            $inlineKeyboard = array();
            foreach ($prod as $product) {
                $bot->sendProduct($chatId, $product);
            }
            $bot->setLastAction($chatId, "");
            $lastAction2 = [
                'action' => 'more_prod',
                'data' => [
                    'count' => $count_next,
                    'sectionID' => $lastAction['data']['sectionID'],
                ],
            ];
            $bot->setLastAction($chatId, $lastAction2);
            if ($check >= $more) {
                $inlineKeyboard_more[] = [array(
                    'text' => 'Показать еще',
                    'callback_data' => '{"type": "more_prod"}'
                )];
                $bot->sendMessage($chatId, $msg, $inlineKeyboard_more, 'HTML', 'inline');
            }
        }
        break;

    default:
//        die();
    break;
}
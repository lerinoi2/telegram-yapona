<?php

class Botan
{
  const BASE_URL = 'https://api.botan.io/track';

  protected $curl;
    //Yandex AppMetrica application api_key
  protected $token;

  public function __construct($token)
  {
    if (empty($token) || !is_string($token)) {
      throw new InvalidArgumentException('Token should be a string');
    }

    $this->token = $token;
    $this->curl = curl_init();
  }

  public function track($message, $eventName = 'Сообщение')
  {
    $uid = $message['from']['id'];

    $options = [
      CURLOPT_URL => self::BASE_URL . "?token={$this->token}&uid={$uid}&name={$eventName}",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_POST => true,
      CURLOPT_HTTPHEADER => [
        'Content-Type: application/json'
      ],
      CURLOPT_POSTFIELDS => json_encode($message)
    ];

    curl_setopt_array($this->curl, $options);
    $result = curl_exec($this->curl);

    if ($result['status'] !== 'accepted') {
      throw new Exception('Error Processing Request');
    }
  }

  public function __destruct()
  {
    $this->curl && curl_close($this->curl);
  }
}
?>
<?php
class newOrder {
    private $data = array();
    public function __construct($server, $user, $pass, $dbname){
        $this->server = $server;
        $this->user = $user;
        $this->pass = $pass;
        $this->dbname = $dbname;
        $this->error = false;
        $this->db = false;
        $this->openConnection();
    }
    public function openConnection()  {  
        if(!$this->db){  
            
            $this->connection = new mysqli($this->server, $this->user, $this->pass, $this->dbname); 
            $this->connection->set_charset("utf8");
            
            if($this->connection){  

                $selectDB = $this->connection;

                if($selectDB){  
                    $this->db = true;
                    return true;  
                } else {  
                    $this->error = 'Не удалось подключиться к базе данных';
                    return false;  
                }  
            } else {  
                $this->error = 'Не удалось подключиться к базе данных';
                return false;  
            }  
        } else {  
            return true;  
        }  
    }
    public function closeConnection() {
        if($this->db)
        {  
            if($this->connection->close())
            {
                $this->db = false;
                return true;
            } else {
                
                return false;
            }
        }
    }  
    public function addOrder($ord){
        if(is_array($ord)){
            if($ord['products']){
                $price = $this->calcSumm($ord['products']);
                if($price > 0){
                    $order = array(
                        'created' => (new \DateTime())->format('Y-m-d H:i:s'), // now()
                        'phone' => $ord['phone'], // From Email
                        'comment' => $ord['comment'] ? $ord['comment'] : '',
                        'price' => $price, // Вычисляется из суммы товаров в заказе
                        'state' => 1,
                    );

                    $order_id = $this->insetOrder($order);

                    if($order_id && $this->addOrderProducts($ord['products'], $order_id)){
                        return $order_id;
                    }else{
                        $this->error = 'Не удалось вписать заказ в базу';
                        return false;
                    }
                }else{
                    $this->error = 'Сумма товаров не больше 0';
                    return false;   
                }
            }else{
                $this->error = 'Нет продуктов во входящих данных';
                return false;
            }
        }else{
            $this->error = 'Входящие данные не массив';
            return false;
        }        
    }
    public function calcSumm($products){
        $summ = 0;
        foreach($products as $product){
            $id = $product['id'];
            $number = $product['number'];
            $arPrice = $this->getProductPrice($id);
            $price = $arPrice->fetch_assoc();
            if($price['price']){
                $summ += $price['price'] * $number;
            }
        }
        return $summ;
    }
    public function getProductIdByCode($code){
        $query = $this->connection->query("SELECT id from product WHERE code = ".$code);
        $id = $query->fetch_assoc();
        return $id['id'];
    }
    public function getProductPrice($id){
        $price = $this->connection->query("SELECT price from product WHERE code = ".$id);
        return $price;
    }
    
    public function insetOrder($order){
        $insert = $this->getAddRequestString($order);
        $ins = $this->connection->query($insert);
        $order_id = $this->connection->insert_id;
        
        return ($ins) ? $order_id : false;
    }
    public function addOrderProducts($products, $order_id){
        foreach($products as $product) {
            $id = $this->getProductIdByCode($product['id']);
            if($id){
                $num = $product['number'];
                $insert = "INSERT INTO `ord_product` (`product`, `ord`, `cnt`, `plate`) VALUES ('$id', '$order_id', '$num', 1)";
                $ins = $this->connection->query($insert);

                if(!$ins) return false;
            }
        }
        return true;
    }
    public function getAddRequestString($ord){
        $created = $ord['created'];
        $phone = $ord['phone'];
        $comment = $ord['comment'];
        $price = $ord['price'];
        
        return "INSERT INTO ord (`created`, `phone`, `comment`, `price`, `state`) VALUES ('$created', '$phone', '$comment', '$price', 1)";
    }

    public function getStatus($phone) {
        $res = $this->connection->query("SELECT `id`,`state` FROM `ord` WHERE `phone`='".$phone."' ORDER BY `id` DESC LIMIT 1")->fetch_assoc();
        return json_encode($res);
    }
}

$JSON = @file_get_contents('php://input');
$inputData = json_decode($JSON, true);


// $newOrder = new newOrder('127.0.0.1', 'ym_nt', 'CRgsJj6R3K', 'ym_nt');
$newOrder = new newOrder('server104.hosting.reg.ru', 'u0296362_user2', 'user2321', 'u0296362_ym');
if ($newOrder) {
    if (isset($inputData['statusPhone'])) {
        $request = $newOrder->getStatus($inputData['statusPhone']);
    } else {
        $request = $newOrder->addOrder($inputData);
    }
    $newOrder->closeConnection();
} else {
    $request = false;
}


if ($request) {
    echo $request;
} else {
    echo 'Error: '.$newOrder->error;
}

?>
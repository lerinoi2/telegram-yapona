<?php
include_once "default.php";
include_once "locale.php"; // Переменная $l

require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");

class Bot extends Def
{
  // кол-во товаров, отображаемых в поиске
  const PAGE_SIZE = 5;
  private $VERSION = "0.02";
  private $unset_predlog = true; 
  private $Stem_Caching = 0;
  private $Stem_Cache = array();
  private $VOWEL = '/аеиоуыэюя/u';
  private $PERFECTIVEGROUND = '/((ив|ивши|ившись|ыв|ывши|ывшись)|((?<=[ая])(в|вши|вшись)))$/u';
  private $REFLEXIVE = '/(с[яь])$/u';
  private $ADJECTIVE = '/(ее|ие|ые|ое|ими|ыми|ей|ий|ый|ой|ем|им|ым|ом|его|ого|еых|ую|юю|ая|яя|ою|ею)$/u';
  private $PARTICIPLE = '/((ивш|ывш|ующ)|((?<=[ая])(ем|нн|вш|ющ|щ)))$/u';
  private $VERB = '/((ила|ыла|ена|ейте|уйте|ите|или|ыли|ей|уй|ил|ыл|им|ым|ены|ить|ыть|ишь|ую|ю)|((?<=[ая])(ла|на|ете|йте|ли|й|л|ем|н|ло|но|ет|ют|ны|ть|ешь|нно)))$/u';
  private $NOUN = '/(а|ев|ов|ие|ье|е|иями|ями|ами|еи|ии|и|ией|ей|ой|ий|й|и|ы|ь|ию|ью|ю|ия|ья|я)$/u';
  private $RVRE = '/^(.*?[аеиоуыэюя])(.*)$/u';
  private $DERIVATIONAL = '/[^аеиоуыэюя][аеиоуыэюя]+[^аеиоуыэюя]+[аеиоуыэюя].*(?<=о)сть?$/u';
  private $PREDLOG = '/(^|\s)(и|для|в|на|под|из|с|по)(\s|$)/u';

  public function addUser($chatId, $name) {
    if (!CIBlockElement::GetList(Array(), Array('IBLOCK_ID'=>11,'PROPERTY_chatId' => $chatId), false, false, Array('ID'))->result->num_rows) {
      $arFields = array(
        "ACTIVE" => "Y",
        "IBLOCK_ID" => 11,
        "IBLOCK_SECTION_ID" => false,
        "NAME" => $name,
        "PROPERTY_VALUES" => array(
          "chatId" => $chatId,
          "platform_id" => "0",
        )
      );
      $el = new CIBlockElement();
      $el->Add($arFields);
    }
  }

  public function historyProd($chatId, $number = 4)
  {
      $arSelectPlatform = Array(
          'ID',
          'PROPERTY_USERID',
      );
      $platform = CIBlockElement::GetList(Array(), Array('IBLOCK_ID'=>11,'PROPERTY_chatId' => $chatId), false, false, $arSelectPlatform)->GetNext();
      if ($platform["PROPERTY_USERID_VALUE"]){
          $arSelectUser = Array(
              'ID',
              'PROPERTY_historyOrder');
          $user = CIBlockElement::GetList(Array(), Array('IBLOCK_ID'=>10,'ID' => $platform["PROPERTY_USERID_VALUE"]), false, false, $arSelectUser)->GetNext();
      }
      if ($number == 4){
          $history = json_decode($user["~PROPERTY_HISTORYORDER_VALUE"]);
          $count = count($history);
          return $count;
      }
      $history = json_decode($user["~PROPERTY_HISTORYORDER_VALUE"])[$number];
      $basket = $history;
      CIBlockElement::SetPropertyValues($this->getUserId($chatId), 11, json_encode($history), "lastAction");

      return $basket;
  }


    public function getSummLastAction($chatId) {
        $basket = $this->getLastAction($chatId);
        $unSale = 0;
        $forSale = 0;
        $total = 0;

        $res = $this->getUserInfoSale($chatId);
        $arSet = array("15524", "15529", "15530", "15533", "15534", "15535");

        foreach ($basket as $basketItem) {
            $product = $this->getProduct($basketItem['id']);
            $summ = $basketItem['c']*$product['PRICES'][$basketItem['o']]['VALUE'];
            if ($res['SALE_SIZE']) {
                if (in_array($basketItem['id'], $arSet)){
                    $unSale += $summ;
                } else {
                    $forSale += $summ;
                }
            } else {
                $unSale += $summ;
            }
        }
        $info = $this->getUserInfo($chatId)["deliveryType"];
        if ($forSale > 0){
            if ($info == 'Самовывоз' and (int)$res['SALE_SIZE'] < 10){
                $sale = (int)$forSale * (1 - 10 / 100);
            } else {
                $sale = (int)$forSale * (1 - (int)$res['SALE_SIZE'] / 100);
            }
            $total = round($sale + $unSale);
        } else {
            if ($info == 'Самовывоз'){
                $unSale = (int)$unSale * (1 - 10 / 100);
            }
            $total = round($unSale);
        }
        return round($total);
    }

  public function getUserNumber($chatId) {
    $user = CIBlockElement::GetList(Array(), Array('IBLOCK_ID'=>10,'ID' => $chatId), false, false, Array('PROPERTY_phone'))->GetNext();
    return  $user["PROPERTY_PHONE_VALUE"];
  }

  public function getLastOrder($chatId){
    $res = $this->getUserInfo($chatId);
    $arSelect = Array('PROPERTY_orderId');
    $user = CIBlockElement::GetList(Array(), Array('IBLOCK_ID'=>10,'ID' => $res['user_id']), false, false, $arSelect)->GetNext();
//    $number_order = [
//      "orderId" => $user["PROPERTY_ORDERID_VALUE"],
//    ];
    $number_order = array(
      "orderId" => $user["PROPERTY_ORDERID_VALUE"],
    );

    return $number_order;
  }


  public function getUserOrder($Id) {
    $arSelect = Array(
      'ID', 
      'PROPERTY_PRODUCTS', 
      'PROPERTY_ADDRESS');
    $user = CIBlockElement::GetList(Array(), Array('IBLOCK_ID'=>7,'ID' => $Id), false, false, $arSelect)->GetNext();

    $arSelect2 = Array('PROPERTY_PRODUCTS');
    $product = CIBlockElement::GetList(Array(), Array('IBLOCK_ID'=>7,'ID' => $Id), false, false, $arSelect2);

    while($arItem = $product->Fetch())
    {
      $arFields[] = $arItem["PROPERTY_PRODUCTS_VALUE"];
    }

    return [
      "id" => $user["ID"],
      "ADDRESS" => $user["PROPERTY_ADDRESS_VALUE"],
      "PRODUCTS" =>$arFields,
    ];
  }

  public function getUserId($chatId) {
    return CIBlockElement::GetList(Array(), Array('IBLOCK_ID'=>11,'PROPERTY_chatId' => $chatId), false, false, Array('ID'))->GetNext()['ID'];
  }

  /* Функции для работы с корзиной */

  public function getBasket($chatId) {
    $basket = json_decode(CIBlockElement::GetProperty(11, $this->getUserId($chatId), array(), Array("CODE"=>"basket"))->Fetch()['VALUE'], TRUE);
    return ($basket == false)?Array():$basket;
  }

  public function setBasket($chatId, $basket) {
    return CIBlockElement::SetPropertyValues($this->getUserId($chatId), 11, json_encode($basket), "basket");
  }

  public function getBasketSumm($chatId) {
      $unSale = 0;
      $forSale = 0;
      $total = 0;

      $basket = $this->getBasket($chatId);
      $res = $this->getUserInfoSale($chatId);
      $arSet = array("15524", "15529", "15530", "15533", "15534", "15535");

      foreach ($basket as $basketItem) {
          $product = $this->getProduct($basketItem['id']);
          $summ = $basketItem['c']*$product['PRICES'][$basketItem['o']]['VALUE'];
          if ($res['SALE_SIZE']) {
              if (in_array($basketItem['id'], $arSet)){
                  $unSale += $summ;
              } else {
                  $forSale += $summ;
              }
          } else {
              $unSale += $summ;
          }
      }
      $info = $this->getUserInfo($chatId)["deliveryType"];
      if ($forSale > 0){
          if ($info == 'Самовывоз' and (int)$res['SALE_SIZE'] < 10){
              $sale = (int)$forSale * (1 - 10 / 100);
          } else {
              $sale = (int)$forSale * (1 - (int)$res['SALE_SIZE'] / 100);
          }
          $total = round($sale + $unSale);
      } else {
          if ($info == 'Самовывоз'){
              $unSale = (int)$unSale * (1 - 10 / 100);
          }
          $total = round($unSale);
      }

      return round($total);
  }

  public function getBasketProduct($chatId, $productId, $productMod) {
    $basket = $this->getBasket($chatId);
    foreach ($basket as $key => $product) {
      if (($product['id'] == $productId) && ($product['o'] == $productMod)) {
        $product['key'] = $key;
        return $product;
      }
    }
    return false;
  }

  public function addProduct($chatId, $productId, $productMod) {
    $basket = $this->getBasket($chatId);
    $product = $this->getBasketProduct($chatId, $productId, $productMod);
    if ($product) {
      $count = $product['c'] + 1;
      $basket[$product['key']]['c'] = $count;
    } else {
      $count = 1;
      $basket[] = [
        "id" => $productId,
        "c" => 1,
        "o" => $productMod,
      ];
    }
    $this->setBasket($chatId, $basket);
    return $count;
  }

  public function removeProduct($chatId, $productId, $productMod) {
    $basket = $this->getBasket($chatId);
    $product = $this->getBasketProduct($chatId, $productId, $productMod);
    if ($product) {
      $count = $product['c'] - 1;
      if ($count == 0) {
        unset($basket[$product['key']]);
      } else {
        $basket[$product['key']]['c'] = $count;
      }
    }
    $this->setBasket($chatId, $basket);
    return $count;
  }

  /* Функции для работы с корзиной - конец */

  /* Функции для работы с меню */

  public function getModId($modText = '') {
    switch ($modText) {
      case "4 ролла": return 1;
      default: return 0;
    }
  }

  // Старая версия меню
  // public function getMenu() {
  //   $sections = CIBlockSection::GetList(Array("SORT"=>"ASC"), Array('IBLOCK_ID' => 1, 'GLOBAL_ACTIVE' => 'Y'));
  //   $res = Array();
  //   while ($sec = $sections->GetNext()) {
  //     if ($sec['DEPTH_LEVEL'] == 1) {
  //       if ($sec['NAME'] == 'Японская кухня') {
  //         $emoji = $this->getEmoji('\xF0\x9F\x87\xAF\xF0\x9F\x87\xB5');
  //       } else if ($sec['NAME'] == 'Традиционная кухня') {
  //         $emoji = $this->getEmoji('\xF0\x9F\x87\xB7\xF0\x9F\x87\xBA');
  //       }
  //       $res[$sec['ID']] = Array('NAME' => $sec['NAME'],'EMOJI' => $emoji, 'ITEMS' => Array());
  //     } else {
  //       $res[$sec['IBLOCK_SECTION_ID']]['ITEMS'][$sec['ID']] = Array('NAME' => $sec['NAME']);
  //     }
  //   }

  //   return $res;
  // }

  // Получение всех категорий из корня
  public function getMenu() {
    $sections = CIBlockSection::GetList(Array("SORT"=>"ASC"), Array('IBLOCK_ID' => 1, 'GLOBAL_ACTIVE' => 'Y', 'DEPTH_LEVEL' => 1));
    $res = Array();
    while ($sec = $sections->GetNext()) {
      if ($sec['NAME'] == 'Японская кухня') {
        $emoji = $this->getEmoji('\xF0\x9F\x87\xAF\xF0\x9F\x87\xB5');
      } else if ($sec['NAME'] == 'Традиционная кухня') {
        $emoji = $this->getEmoji('\xF0\x9F\x87\xB7\xF0\x9F\x87\xBA');
      }
      $res[$sec['ID']] = Array('NAME' => $sec['NAME'],'EMOJI' => $emoji);
    }

    return $res;
  }

  // Получение всех категорий (вложенность - 2)
  public function getSection($sectionId) {
    $sections = CIBlockSection::GetList(Array("SORT"=>"ASC"), Array('IBLOCK_ID' => 1, 'GLOBAL_ACTIVE' => 'Y', 'SECTION_ID' => $sectionId), false, Array('ID','NAME'));
    $res = Array();
    while ($sec = $sections->GetNext()) {
      $res[$sec['ID']] = Array('NAME' => $sec['NAME']);
    }
    return $res;
  }

  // Получение товаров из категории
  public function getProducts($sectionId) {
    $products = CIBlockElement::GetList(
      Array("SORT"=>"ASC"), 
      Array('IBLOCK_ID' => 1, 'ACTIVE' => 'Y', 'SECTION_ID' => $sectionId), 
      false, false, 
      Array('ID', 'NAME', 'DETAIL_PICTURE', 'PROPERTY_WEIGHT', 'PROPERTY_SOSTAV')
    );
    $res = Array(
      'SECTION' => CIBlockSection::GetByID($sectionId)->GetNext()['NAME'],
      'ITEMS' => Array()
    );
    while ($product = $products->GetNext()) {
      $props = CIBlockElement::GetProperty(1, $product['ID'], array(), array('CODE' => 'PRICE'));
      while ($prop = $props->Fetch()) {
        $product['PRICES'][] = array(
          "NAME" => $prop["DESCRIPTION"],
          "VALUE" => $prop["VALUE"]
        );
      }
      $res['ITEMS'][] = Array(
        'ID' => $product['ID'],
        'NAME' => $product['NAME'],
        'IMG' => $_SERVER["SERVER_NAME"].CFile::GetPath($product['DETAIL_PICTURE']),
        'PRICES' => $product['PRICES'],
        'SOSTAV' => $product['PROPERTY_SOSTAV_VALUE'],
        'WEIGHT' => $product['PROPERTY_WEIGHT_VALUE'],
      );
    }

    return $res;
  }

  // Получение одного товара
  public function getProduct($productId) {
    $product = CIBlockElement::GetList(
      Array(), 
      Array('IBLOCK_ID' => 1, 'ACTIVE' => 'Y', 'ID' => $productId), 
      false, false, 
      Array('ID', 'NAME', 'DETAIL_PICTURE', 'PROPERTY_WEIGHT', 'PROPERTY_SOSTAV')
    )->GetNext();

    $props = CIBlockElement::GetProperty(1, $productId, array(), array('CODE' => 'PRICE'));
    while ($prop = $props->Fetch()) {
      $product['PRICES'][] = array(
        "NAME" => $prop["DESCRIPTION"],
        "VALUE" => $prop["VALUE"]
      );
    }

    $res = Array(
      'ID' => $product['ID'],
      'NAME' => $product['NAME'],
      'IMG' => $_SERVER["SERVER_NAME"].CFile::GetPath($product['DETAIL_PICTURE']),
      'PRICES' => $product['PRICES'],
      'SOSTAV' => $product['PROPERTY_SOSTAV_VALUE'],
      'WEIGHT' => $product['PROPERTY_WEIGHT_VALUE'],
    );

    return $res;
  }


  public function check($str)
  {
    $checker=json_decode(file_get_contents("http://speller.yandex.net/services/spellservice.json/checkText?text=".urlencode($str)));
    $checked_str=$str;
    foreach($checker as $word) {
      if (!empty($word->s)){
          $checked_str=str_replace($word->word,$word->s[0],$checked_str);
        }
    }
    if(mb_strtolower($checked_str,'utf8')!=mb_strtolower($str,'utf8') && !empty($checked_str)) {
      $msg = "Возможно вы имели ввиду: ".$checked_str;
      return $msg;
    }
  }
  private function s(&$s, $re, $to) {
    $orig = $s;
    $s = preg_replace($re, $to, $s);
    return $orig !== $s;
  }

  private function m($s, $re) {
    return preg_match($re, $s);
  }

  public function stem_string($words) { 
    $word = explode(' ', $words);
    for ($i = 0; $i <= count($word); $i++) {
      if ($this->unset_predlog === TRUE)
        $word[$i] = preg_replace($this->PREDLOG, '', $word[$i]);
      $word[$i] = $this->stem_word($word[$i]);
      if (empty($word[$i]))
        unset($word[$i]);
    }
    return implode(' ', $word); 
  }

  private function stem_word($word) {
    mb_regex_encoding('UTF-8');
    mb_internal_encoding('UTF-8');
    $word = mb_strtolower($word);
    $word = str_ireplace('ё', 'е', $word);

    if ($this->Stem_Caching && isset($this->Stem_Cache[$word])) {
      return $this->Stem_Cache[$word];
    }

    $stem = $word;
    do {
      if (!preg_match($this->RVRE, $word, $p))
        break;
      $start = $p[1];
      $RV = $p[2];
      if (!$RV)
        break;


      if (!$this->s($RV, $this->PERFECTIVEGROUND, '')) {
        $this->s($RV, $this->REFLEXIVE, '');

        if ($this->s($RV, $this->ADJECTIVE, '')) {
          $this->s($RV, $this->PARTICIPLE, '');
        } else {
          if (!$this->s($RV, $this->VERB, ''))
            $this->s($RV, $this->NOUN, '');
        }
      }


      $this->s($RV, '/и$/', '');


      if ($this->m($RV, $this->DERIVATIONAL))
        $this->s($RV, '/ость?$/', '');

      if (!$this->s($RV, '/ь$/', '')) {
        $this->s($RV, '/ейше?/', '');
        $this->s($RV, '/нн$/', 'н');
      }

      $stem = $start . $RV;
    } while (false);
    if ($this->Stem_Caching)
      $this->Stem_Cache[$word] = $stem;
    return $stem;
  }

  private function stem_caching($parm_ref) {
    $caching_level = @$parm_ref['-level'];
    if ($caching_level) {
      if (!$this->m($caching_level, '/^[012]$/')) {
        die(__CLASS__ . "::stem_caching() - Legal values are '0','1' or '2'. '$caching_level' is not a legal value");
      }
      $this->Stem_Caching = $caching_level;
    }
    return $this->Stem_Caching;
  }

  public function clear_stem_cache() {
    $this->Stem_Cache = array();
  }

  public function find($query) {
      $search_str = preg_replace("/[^а-яА-Яa-zA-z0-9\-]/ui"," ",$query);
      $arOrder = array();
      $arSelect = Array("ID", "NAME", "PROPERTY_ORDER_NUMBER", "PROPERTY_ORDER_NUMBER");
      $arFilter = Array(
          "IBLOCK_ID"=>1,
          'ACTIVE' => 'Y',
          "NAME"=> "%".$search_str."%",
      );
      $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
      while($items = $res->GetNext()) {
          $products[] = [
              'ID' => $items['ID'],
              'NAME' => $items['NAME'],
              'SOSTAV' => $items['PROPERTY_SOSTAV_VALUE'],
              'kol' => $items['PROPERTY_ORDER_NUMBER_VALUE']
          ];
      }

      $stem_str = $this->stem_string($query);
      $search_str = preg_replace("/[^а-яА-Яa-zA-z0-9\-]/ui"," ",$stem_str);
      $arQuery = explode(" ", $search_str);
      foreach ($arQuery as $key => $value) {
          $val[] = "%".$value."%,";
      }
      $val2 = implode($val2, $val);
      $val2 = substr($val2, 0, -1);
      foreach ($arQuery as $key => $value) {
          $arOrder = array("PROPERTY_ORDER_NUMBER" => "DESC");
          $arSelect = Array("ID", "NAME", "PROPERTY_SOSTAV", "PROPERTY_ORDER_NUMBER", "PROPERTY_ORDER_NUMBER");
          $arFilter = Array(
              "IBLOCK_ID"=>1,
              'ACTIVE' => 'Y',
              array(
                  "LOGIC" => "OR",
                  "NAME"=>"%".$value."%",
                  "PROPERTY_SOSTAV"=>$val2
              )
          );
          $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
          while($item = $res->GetNext()) {
              if ($item['ID'] == $products[0]['ID']){
                  continue;
              } else {
                  $products[] = [
                      'ID' => $item['ID'],
                      'NAME' => $item['NAME'],
                      'SOSTAV' => $item['PROPERTY_SOSTAV_VALUE'],
                      'kol' => $item['PROPERTY_ORDER_NUMBER_VALUE']
                  ];
              }
          }
          $sections = array();
          $arSelect = Array("ID", "NAME");
          $arFilter = Array(
              "IBLOCK_ID"=>1,
              "NAME"=>"%".$value."%",
          );
          $res = CIBlockSection::GetList(Array("NAME"=>"ASC", 'ACTIVE' => 'Y'), $arFilter, false, false, $arSelect);
          while($item = $res->GetNext()) {
              $sections[] = [
                  'ID' => $item['ID'],
                  'NAME' => $item['NAME'],
              ];
          }
      }
      $countProducts = count($products);
      $countSections = count($sections);
      if (($countProducts == 0) and ($countSections == 0)) {
          $check = $this->check($query);
          if ($check){
              return [
                  'msg' => $check
              ];
          }
      } else {
          return [
              'products' => $products,
              'sections' => $sections,
          ];
      }
  }

  /* Функции для работы с меню - конец */

  /* Функции для работы с информацией о пользователе */

  public function getUserInfo($chatId) {
    $arSelectPlatform = Array(
      'ID', 
      'NAME',
      'PROPERTY_USERID',
      'PROPERTY_PLATFORM_ID',
    );
    $platform = CIBlockElement::GetList(Array(), Array('IBLOCK_ID'=>11,'PROPERTY_chatId' => $chatId), false, false, $arSelectPlatform)->GetNext();

    if ($platform["PROPERTY_USERID_VALUE"]){
      $arSelectUser = Array(
        'ID', 
        'PROPERTY_phone', 
        'PROPERTY_address', 
        'PROPERTY_person', 
        'PROPERTY_payType', 
        'PROPERTY_deliveryType', 
        'PROPERTY_orderStatus', 
//        'PROPERTY_cafe',
        'PROPERTY_USER_FIO',
        'PROPERTY_SALE_SIZE',
        'PROPERTY_BirthDay',);
      $user = CIBlockElement::GetList(Array(), Array('IBLOCK_ID'=>10,'ID' => $platform["PROPERTY_USERID_VALUE"]), false, false, $arSelectUser)->GetNext();
    }
    return [
      "id" => $platform["ID"],
      "name" => $platform["NAME"],
      "phone" => $user["PROPERTY_PHONE_VALUE"]?$user["PROPERTY_PHONE_VALUE"]:false,
      "person" => $user["PROPERTY_PERSON_VALUE"]?$user["PROPERTY_PERSON_VALUE"]:false,
      "payType" => $user["PROPERTY_PAYTYPE_VALUE"]?$user["PROPERTY_PAYTYPE_VALUE"]:false,
      "deliveryType" => $user["PROPERTY_DELIVERYTYPE_VALUE"]?$user["PROPERTY_DELIVERYTYPE_VALUE"]:false,
      "address" => ($user["PROPERTY_DELIVERYTYPE_VALUE"] == 'Доставка' || $user["PROPERTY_ADDRESS_VALUE"])?$user["PROPERTY_ADDRESS_VALUE"]:0,
//      "cafe" => ($user["PROPERTY_DELIVERYTYPE_VALUE"]=='Самовывоз' || $user["PROPERTY_CAFE_VALUE"])?$user["PROPERTY_CAFE_VALUE"]:0,
      "user_id" => $platform["PROPERTY_USERID_VALUE"]?$platform["PROPERTY_USERID_VALUE"]:0,
      "orderStatus" => $user["PROPERTY_ORDERSTATUS_VALUE"]?$user["PROPERTY_ORDERSTATUS_VALUE"]:0,
    ];
  }

  public function getUserInfoSale($chatId) {
    $arSelectPlatform = Array(
      'ID', 
      'NAME',
      'PROPERTY_USERID',
      'PROPERTY_PLATFORM_ID',
    );
    $platform = CIBlockElement::GetList(Array(), Array('IBLOCK_ID'=>11,'PROPERTY_chatId' => $chatId), false, false, $arSelectPlatform)->GetNext();

    if ($platform["PROPERTY_USERID_VALUE"]){
      $arSelectUser = Array(
        'ID', 
        'PROPERTY_phone', 
        'PROPERTY_address', 
        'PROPERTY_person', 
        'PROPERTY_payType', 
        'PROPERTY_deliveryType', 
        'PROPERTY_orderStatus', 
        'PROPERTY_cafe',
        'PROPERTY_USER_FIO',
        'PROPERTY_SALE_SIZE',
        'PROPERTY_SALE_NAME',
        'PROPERTY_BirthDay',);
      $user = CIBlockElement::GetList(Array(), Array('IBLOCK_ID'=>10,'ID' => $platform["PROPERTY_USERID_VALUE"]), false, false, $arSelectUser)->GetNext();
    }
    return [
      "USER_FIO" => $user["PROPERTY_USER_FIO_VALUE"]?$user["PROPERTY_USER_FIO_VALUE"]:false,
      "SALE_SIZE" => $user["PROPERTY_SALE_SIZE_VALUE"]?$user["PROPERTY_SALE_SIZE_VALUE"]:false,
      "BirthDay" => $user["PROPERTY_BIRTHDAY_VALUE"]?$user["PROPERTY_BIRTHDAY_VALUE"]:false,
        "SALE_NAME" => $user["PROPERTY_SALE_NAME_VALUE"]?$user["PROPERTY_SALE_NAME_VALUE"]:false,
        "phone" => $user["PROPERTY_PHONE_VALUE"]?$user["PROPERTY_PHONE_VALUE"]:false,
      
    ];
  }

  public function getEmptyUserInfo($chatId) {
    $user = $this->getUserInfo($chatId);
    foreach ($user as $code => $value) {
      if ($value === false || is_null($value)) {
        return $code;
      }
    }
    return false;
  }



  public function setUserInfo($chatId, $code, $value, $sec = 10) {
    if ($code == 'phone') {
      $value = str_replace("+7", "8", $value); 
      $value = preg_replace('/(\(|\)|-|\s|\s+)/', '', $value);
      if($value[0] == "7") {
        $value = "8".substr($value, 1);
      }
    }

    if ($sec == 10){
      $user_id = $this->getUserInfo($chatId);
    }else{
      $user_id['user_id'] = $this->getUserId($chatId);
    }

    if ($user_id['user_id'] === 0){
      $check = 2;
      $res = CIBlockElement::GetList(Array(), Array('IBLOCK_ID'=>10), false, false, Array('ID', "PROPERTY_".$code));
      while($ob = $res->GetNextElement()){ 
        $qwe = "PROPERTY_".strtoupper($code)."_VALUE";
         if ($ob->fields[$qwe] == $value){
          $userID = $ob->fields["ID"];
          $id_11 = CIBlockElement::GetList(Array(), Array('IBLOCK_ID'=>11, 'PROPERTY_chatId' => $chatId), false, false, Array('ID'))->Fetch()['ID'];
          CIBlockElement::SetPropertyValues($id_11, 11, $userID, "userid");
          $check = 1;       
         }
      }
      if ($check == 2){
        $arFields = array(
          "ACTIVE" => "Y",
          "IBLOCK_ID" => 10,
          "IBLOCK_SECTION_ID" => false,
          "NAME" => "USER",
          "PROPERTY_VALUES" => array(
            $code => $value,
          )
        );
        $el = new CIBlockElement();
        $el->Add($arFields);
        $id_10 = CIBlockElement::GetList(Array(), Array('IBLOCK_ID'=>10, "PROPERTY_".$code => $value), false, false, Array('ID'))->Fetch()['ID'];
        $id_11 = CIBlockElement::GetList(Array(), Array('IBLOCK_ID'=>11, 'PROPERTY_chatId' => $chatId), false, false, Array('ID'))->Fetch()['ID'];
        CIBlockElement::SetPropertyValues($id_11, 11, $id_10, "userid");
      }
    }  
    else{
      return CIBlockElement::SetPropertyValues($user_id['user_id'], $sec, $value, $code);
    } 
  }

  public function getInfoKeyboardSale() {
    return [
      [[
        'text' => ' Телефон ',
        'callback_data' => '{"type":"changeInfoSale","code":"phone"}',
      ]],
      [[
        'text' => ' ФИО ',
        'callback_data' => '{"type":"changeInfoSale","code":"USER_FIO"}',
      ]],
      [[
        'text' => ' Дата рождения ',
        'callback_data' => '{"type":"changeInfoSale","code":"BirthDay"}',
      ]],
    ];
  }

  public function getInfoKeyboard() {
    return [
      [[
        'text' => ' Телефон ',
        'callback_data' => '{"type":"changeInfo","code":"phone"}',
      ]],
      [[
        'text' => ' Адрес ',
        'callback_data' => '{"type":"changeInfo","code":"address"}',
      ]],
      [[
        'text' => ' Кол-во персон ',
        'callback_data' => '{"type":"changeInfo","code":"person"}',
      ]],
      [[
        'text' => ' Способ оплаты ',
        'callback_data' => '{"type":"changeInfo","code":"payType"}',
      ]],
      [[
        'text' => ' Способ доставки ',
        'callback_data' => '{"type":"changeInfo","code":"deliveryType"}',
      ]],

    ];

//      [[
//        'text' => ' Адрес ближайшего кафе ',
//        'callback_data' => '{"type":"changeInfo","code":"cafe"}',
//      ]],
  }
  
  public function getInfoDataSale($code) {
    global $l;

    switch ($code) {
      case "phone": 
        return [
          "message" => "<b>Укажите Ваш номер телефона:</b> \nНажмите \"Отправить номер\" или введите Ваш номер телефона.",
          "keyboard" => [[["text" => $l['telBtn'], "request_contact" => true], $l['backBtn']]],
          "keyboardType" => false,
        ];
      case "USER_FIO": 
      return [
        "message" => "Пожалуйста, введите Ваши Имя и Фамилию.",
        "keyboard" => [[$l['backBtn']]],
        "keyboardType" => false,
      ];
      case "BirthDay": 
      return [
        "message" => "Пожалуйста, введите Вашу дату рождения в формате дд.мм.гг",
        "keyboard" => [[$l['backBtn']]],
        "keyboardType" => false,
      ];
      default: return false;
    }
  }

  public function getInfoData($code, $action = 'set') {
    global $l;

    switch ($code) {
      case "phone": 
        return [
          "message" => "<b>Укажите Ваш номер телефона:</b> \nНажмите \"Отправить номер\" или введите Ваш номер телефона.",
          "keyboard" => [[["text" => $l['telBtn'], "request_contact" => true], $l['resetBtn']]],
          "keyboardType" => false,
        ];
      case "address": 
        return [
          "message" => "<b>Укажите Ваш адрес: </b>\nВведите адрес в формате Улица, дом, подъезд, этаж, квартира.",
          "keyboard" => [[$l['resetBtn']]],
          "keyboardType" => false,
        ];
      case "person": 
        return [
          "message" => "<b>На сколько персон везти приборы?</b>",
          "keyboard" => [[$l['resetBtn']]],
          "keyboardType" => false,
        ];
      case "payType": 
        return [
          "message" => "<b>Укажите тип оплаты:</b>",
          "keyboard" => [[
            ["text" => "Наличными ".$this->getEmoji('\xF0\x9F\x92\xB5'), 'callback_data' => '{"type":"'.$action.'Info","code":"payType","value":32}'],
            ["text" => "Картой ".$this->getEmoji('\xF0\x9F\x92\xB3'), 'callback_data' => '{"type":"'.$action.'Info","code":"payType","value":33}'],
             ["text" => "Онлайн ".$this->getEmoji('\xF0\x9F\x92\xB8'), 'callback_data' => '{"type":"'.$action.'Info","code":"payType","value":34}'],
          ]],
          "keyboardType" => 'inline',
        ];
      case "deliveryType": 
        return [
          "message" => "<b>Укажите тип заказа:</b>",
          "keyboard" => [[
            ["text" => "Доставка ".$this->getEmoji('\xF0\x9F\x9A\x97'), 'callback_data' => '{"type":"'.$action.'Info","code":"deliveryType","value":31}'],
            ["text" => "Самовывоз ".$this->getEmoji('\xF0\x9F\x9A\xB6'), 'callback_data' => '{"type":"'.$action.'Info","code":"deliveryType","value":30}'],
          ]],
          "keyboardType" => 'inline',
        ];

      default: return false;
    }
      //      case "cafe":
//        return [
//          "message" => "<b>Выберите кафе из которого Вы хотите забрать заказ:</b>",
//          "keyboard" => $this->getCafe($action),
//          "keyboardType" => 'inline',
//        ];
  }

  public function getCafe($action) {
    $res = CIBlockElement::GetList(Array("NAME"=>"ASC"), Array("IBLOCK_ID"=>3,"ACTIVE"=>"Y"), false, false, Array('ID', 'NAME'));
    $arrCafe = [];
    while($item = $res->GetNext()) {
      if ($item['ID'] != 181) {
        $arrCafe[] = [["text" => $item['NAME'], 'callback_data' => '{"type":"'.$action.'Info","code":"cafe","value":'.$item['ID'].'}']];
      }
    }

    return $arrCafe;
  }

  public function getCafeName($id) {
    return CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>3,"ID"=>$id), false, false, Array('NAME'))->GetNext()['NAME'];
  }

  public function getLastAction($chatId) {
    $lastAction = json_decode(CIBlockElement::GetProperty(11, $this->getUserId($chatId), array(), Array("CODE"=>"lastAction"))->Fetch()['VALUE'], TRUE);
    return ($lastAction == false)?Array():$lastAction;
  }

  public function setLastAction($chatId, $lastAction) {
    return CIBlockElement::SetPropertyValues($this->getUserId($chatId), 11, json_encode($lastAction), "lastAction");
  }

  /* Функции для работы с информацией о пользователе - конец */

  /* Функции для оформления заказа */

  public function updateSumm($chatId, $lastAction) {
    global $l;

    $res = $this->editMessageText($chatId, $lastAction['data']['summId'], 'Сумма: <b>'.$this->getBasketSumm($chatId).' руб.</b>', null, 'HTML');
    if (!$res) {
      $this->deleteMessage($chatId, $lastAction['data']['summId']);
      $keyboard = [[$l['menuBtn'],$l['orderBtn']]];
      $summId = $this->sendMessage($chatId, 'Сумма: <b>'.$this->getBasketSumm($chatId).' руб.</b>', $keyboard, 'HTML')['message_id'];

      $lastAction = [
        'action' => $lastAction['action'],
        'data' => [
          'titleId' => $lastAction['data']['titleId'],
          'summId' => $summId,
        ],
        'back' => ''
      ];
      $this->setLastAction($chatId, $lastAction);
    }
  }

    public function declOfNum($number, $titles) {
        $cases = array (2, 0, 1, 1, 1, 2);
        return $titles[ ($number%100 > 4 && $number %100 < 20) ? 2 : $cases[min($number%10, 5)] ];
    }

  public function getPayCode($text) {
    switch ($text) {
      case 'Онлайн':
        return 'online';
      case 'Картой':
        return 'card';
      default:
        return 'cash';
    }
  }

  public function createOrder($chatId, $basket, $comment = "") {

    $user = $this->getUserInfo($chatId);
    $sale = $this->getUserInfoSale($chatId);
    $sale_count = false;

    if($sale['SALE_SIZE']){
      $sale_count = true;
    }

    $dtype = $user['deliveryType'] == 'Самовывоз' ? 'pickup' : 'delivery';


      $data = [
          'persons' => $user['person'],
          'phone' => $user['phone'],
          'address' => $user['address'],
          'way' => $this->getPayCode($user['payType']),
          'basket' => json_encode($basket),
          'pas' => 'Iy476m1ww6',
          'comment' => 'Заказ сделан через Telegram. '.$comment,
          'sale' => $sale_count,
          'source' => '2',
          'dtype' => $dtype,
      ];

    $options = [
      CURLOPT_URL => 'https://www.auto-sushi.com/ajax/make-order.php',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_POST => true,
      CURLOPT_POSTFIELDS => $data,
    ];
    $curl = curl_init();
    curl_setopt_array($curl, $options);
    $res = curl_exec($curl);

        $res1 = json_decode($res);

      foreach ($res1 as $key => $value) {
          $ar[$key] = $value;
      }

    if ($ar['result'] == 'success') {
      $this->setBasket($chatId, []);
      CIBlockElement::SetPropertyValues($user["user_id"], 10, $ar['orderId'], 'orderId');
      CIBlockElement::SetPropertyValues($user["user_id"], 10, '1', 'orderStatus');
      CIBlockElement::SetPropertyValues($user["id"], 11, '1', 'check_platform');
      $this->getHistory($user["user_id"], '['.json_encode($basket).']');
    }
    curl_close($curl);
    return $ar;
  }

  public function getHistory($id, $text)
  {
    $arFilter = Array(
      'IBLOCK_ID'=>10,
      "ID" => $id,
    );

    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("ID", "PROPERTY_historyOrder"));
    while($ob = $res->GetNextElement()){ 
      $text_prev =  json_decode($ob->fields["~PROPERTY_HISTORYORDER_VALUE"]); 
      if (!is_null($text_prev)){
        $ar = array_merge($text_prev, json_decode($text));
        $count = count($ar);
        if ($count > 3){
          array_shift($ar);
        } 
        CIBlockElement::SetPropertyValues($id, 10, json_encode($ar), "historyOrder");     
      } else{
        CIBlockElement::SetPropertyValues($id, 10, $text, "historyOrder");          
      }   

    }   
  }

  /* Функции для оформления заказа - конец */

  /* Работа со статусами */

  public function getStatusText($id) {

    /*
    |  1 | Интернет   |
    |  2 | Создан     |
    |  3 | Выгружен   |
    |  4 | Принят     |
    |  5 | Распечатан |
    |  6 | Готов      |
    |  7 | Отгружен   |
    |  8 | Доставлен  |
    |  9 | Отказ      |
    | 10 | Выдано     |
    */

    if ($id < 6) {
      return 'готовится';
    } elseif ($id >= 6 && $id <= 7) {
      return 'в пути';
    } else {
      return 'доставлен';
    }
  }

  // Запрос статуса из aimar
  public function getOrderStatus($chatId) {
        $res = $this->getUserInfo($chatId);
        $phone = $this->getUserNumber($res['user_id']);
        $phone = str_replace("+7", "8", $phone);
        $phone = preg_replace('/(\(|\)|-|\s|\s+)/', '', $phone);
        if($phone[0] == "7") {
           $phone = "8".substr($phone, 1);
        }
        // $phone = CIBlockElement::GetProperty(10, $this->getUserId($chatId), array(), Array("CODE"=>"phone"))->Fetch()['VALUE'];
        include $_SERVER['DOCUMENT_ROOT'].'/aimar/AIMARConnect.php';
        $res = new AIMARConnect();
        $res = json_decode($res->getStatus($phone), true);
        $this->setUserInfo($chatId, 'aimarId', $res['id']);
        $this->setUserInfo($chatId, 'orderStatus', $res['state']);
        return $res['state'];
  }

  /* Работа со статусами - конец */

  // Работа с отзывами
  public function setReview($chatId, $text, $order = false) {
    $arSelect = Array('ID', 'PROPERTY_orderId', 'PROPERTY_aimarId');
    $user = CIBlockElement::GetList(Array(), Array('IBLOCK_ID'=>11,'PROPERTY_chatId' => $chatId), false, false, $arSelect)->GetNext();
    $user = [
      "id" => $user["ID"],
      "orderId" => $user["PROPERTY_ORDERID_VALUE"],
      "aimarId" => $user["PROPERTY_AIMARID_VALUE"],
    ];

    $text_more = $this->getLastAction($chatId);

    if ($text_more['data']['type'] == 'text_more'){
      $arFilter = Array(
        'IBLOCK_ID'=>9,
        "PROPERTY_userId" => $user['id'],
      );

      $res = CIBlockElement::GetList(Array('ID' => 'DESC'), $arFilter, false, false, Array("ID", "PROPERTY_text"));
      while($ob = $res->GetNextElement()){ 
        $rewiev_prev =  $ob->fields["PROPERTY_TEXT_VALUE"]["TEXT"]; 

        $rewiev_text = $rewiev_prev."  ".$text;

        CIBlockElement::SetPropertyValues($ob->fields["ID"], 9, $rewiev_text, "text");
        break;
      }
    }

    if ($text_more['data']['type'] == 'pic'){
      $arFilter = Array(
        'IBLOCK_ID'=>9,
        "PROPERTY_userId" => $user['id'],
      );

      $res = CIBlockElement::GetList(Array('ID' => 'DESC'), $arFilter, false, false, Array("ID", "PROPERTY_photo_review"));
      while($ob = $res->GetNextElement()){ 
        $img_prev =  $ob->fields["PROPERTY_PHOTO_REVIEW_VALUE"]["TEXT"];  
        $img_text = $img_prev."  ".$text_more['data']['path'];
        CIBlockElement::SetPropertyValues($ob->fields["ID"], 9, $img_text, "photo_review");
        break;
      }     
    }

    if (is_int($text)) 
    {
      $arFields = array(
        "ACTIVE" => "Y",
        "IBLOCK_ID" => 9,
        "IBLOCK_SECTION_ID" => false,
        "NAME" => 'Отзыв',
        "PROPERTY_VALUES" => array(
          "userId" => $user['id'],    
          "orderId" => $user['orderId'],
              "messenger" => "viber",
          "aimarId" => $user['aimarId'],

        )
      );
      $el = new CIBlockElement();
        $arFields['PROPERTY_VALUES']['mark'] = $text;
        $el->Add($arFields);
    } 
    else 
    {
      $arFilter = Array(
        'IBLOCK_ID'=>9,
        array(
          "LOGIC" => "AND",
          "PROPERTY_text" => false,
          "PROPERTY_userId" => $user['id'],
          
        )
      );

      $id = CIBlockElement::GetList(Array('ID' => 'DESC'), $arFilter, false, false, Array('ID'))->Fetch()['ID'];

      CIBlockElement::SetPropertyValues($id, 9, $text, "text");
    }
    return $id;
  }

  /* View для повторяющихся сообщений */

  public function getBasketProductView($productId, $productMod, $productCount) {
    $product = $this->getProduct($productId);
    $modText = $product['PRICES'][$productMod]['NAME']==''?'':' (<i>'.$product['PRICES'][$productMod]['NAME'].'</i>)';
    $IMG = $product['IMG'];
    $msg = $product['NAME'].$modText."\n";
    $msg .= "Цена: <b>".$product['PRICES'][$productMod]['VALUE']."</b> руб. \n";
    $msg .= "Кол-во: <b>".$productCount."</b> шт.";
    $inlineKeyboard[0] = [
      array(
        'text' => $this->getEmoji('\xE2\x9E\x95').' Заказать ещё ',
        'callback_data' => '{"type":"orderAdd","productId":'.$product['ID'].',"mod":'.$productMod.',"update":1}',
      ),
      array(
        'text' => ($productCount>1)?$this->getEmoji('\xE2\x9E\x96').' Убавить кол-во ':$this->getEmoji('\xE2\x9C\x96').' Удалить из корзины ',
        'callback_data' => '{"type":"orderRemove","productId":'.$product['ID'].',"mod":'.$productMod.',"update":1}',
      )
    ];

    // $this->sendPhoto($chatId, $product['IMG'], $keyboard);

    return [
      'text' => $msg,
      'keyboard' => $inlineKeyboard,
      'IMG' => $IMG,
    ];
  }
    public function basketTolastAction($chatId, $basket)
    {
        $this->setBasket($chatId, $basket);
    }

    public function getHistoryBasketProductView($productId, $productMod, $productCount) {
        $product = $this->getProduct($productId);
        $modText = $product['PRICES'][$productMod]['NAME']==''?'':' (<i>'.$product['PRICES'][$productMod]['NAME'].'</i>)';
        $IMG = $product['IMG'];
        $msg = $product['NAME'].$modText."\n";
        $msg .= "Цена: <b>".$product['PRICES'][$productMod]['VALUE']."</b> руб. \n";
        $msg .= "Кол-во: <b>".$productCount."</b> шт.";
        return [
            'text' => $msg,
            'IMG' => $IMG,
        ];
    }


    public function getBasketCount($chatId) {
    $basket = $this->getBasket($chatId);
    $count = 0;
    foreach ($basket as $basketItem) {
      $product = $this->getProduct($basketItem['id']);
      $count += $basketItem['c'];
    }
    return $count;
  }

  public function getOrderView($chatId) {
    global $l;
    
    $msg = "<b>Оформление заказа</b> \n\n";

    $user = $this->getUserInfo($chatId);
    $msg .= "<i>Имя:</i> ".($user['name']?$user['name']:'Не указано')." \n";
    $msg .= "<i>Телефон:</i> ".($user['phone']?$user['phone']:'Не указан')." \n";
    $msg .= "<i>Адрес:</i> ".($user['address']?$user['address']:'Не указан')." \n";
    $msg .= "<i>Кол-во персон:</i> ".($user['person']?$user['person']:'Не указано')." \n";
    $msg .= "<i>Способ оплаты:</i> ".($user['payType']?$user['payType']:'Не указан')." \n";
    $msg .= "<i>Способ доставки:</i> ".($user['deliveryType']?$user['deliveryType']:'Не указан')." \n";
//    $msg .= "<i>Адрес ближайшего кафе:</i> ".($user['cafe']?$this->getCafeName($user['cafe']):'Не указан')." \n\n";
    $msg .= 'Сумма: <b>'.$this->getBasketSumm($chatId).' руб. (с учётом скидки)</b>';

    $keyboard = [[$l['sendBtn'],$l['basketBtn']]];
    // $keyboard_inline = [[$l['contactBtn']]];

    return [
      'text' => $msg,
      'keyboard' => $keyboard,
    ];
  }

  /* View для повторяющихся сообщений - конец */

  /* Функции для повторяющихся действий */

  public function sendDefaultChange($chatId) {
    global $l;
        
    // $msg = "<b>Желаете изменить что-нибудь ещё?</b>";
    $msg = $l['changeInfoText1'];
    $keyboard = [[$l['orderBtn'],$l['basketBtn']]];
    $this->sendMessage($chatId, $msg, $keyboard, 'HTML');
    // $msg = "Если нет, то нажмите кнопку \"Оформить заказ\"";
    $msg = $l['changeInfoText2'];
    $keyboard = $this->getInfoKeyboard();
    $this->sendMessage($chatId, $msg, $keyboard, 'HTML', 'inline');
  }

  public function sendProduct($chatId, $product, $keyboard) {
    $msg = "<b>".$product['NAME']."</b>\n\n";
    $msg .= "<i>Cостав: </i> ".$product['SOSTAV']."\n";
    $msg .= "<i>Вес: </i> ".$product['WEIGHT']." г.\n";
    $keyOrder = array();
    foreach ($product['PRICES'] as $prop) {
      $keyOrder[0][] = [
        'text' => ' Заказать '.$prop['NAME'].' ',
        'callback_data' => '{"type":"orderAdd","productId":'.$product['ID'].',"mod":'.$this->getModId($prop['NAME']).',"update":0}'
      ];
      $msg .= "<i>Цена".($prop['NAME']?' за ':'').$prop['NAME'].": </i> ".$prop['VALUE']." руб.\n";
    }

    $this->sendPhoto($chatId, $product['IMG'], $keyboard);
    $this->sendMessage($chatId, $msg, $keyOrder, 'HTML', 'inline');
  }

  /* Функции для повторяющихся действий - конец */

    public function getSale($chatId){
        $res = $this->getUserInfo($chatId);
        $phone = $this->getUserNumber($res['user_id']);
//        $phone = '89028377393';
        $phone = str_replace("+7", "8", $phone);
        $phone = preg_replace('/(\(|\)|-|\s|\s+)/', '', $phone);
        if($phone[0] == "7") {
            $phone = "8".substr($phone, 1);
        }
        include $_SERVER['DOCUMENT_ROOT'].'/aimar/AIMARConnect.php';
        $res = new AIMARConnect();
        $res = json_decode($res->getCard($phone), true);
        return [
            'saleSize' => $res["disc_val"],
            'name' => $res["name"],
        ];
    }
}
?>